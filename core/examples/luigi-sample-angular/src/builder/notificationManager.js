var __Notificationmanager = function(timeout, scope) {
    this.scope = scope;
	this.ng_timeout = timeout;
    this.counter = 0;
    this.dialogStack = [];
    this.scope.notifications = [];
    this.notificationTimeout = 3000;
    this.scope.modalConfiguration = {};
    this.scope.translate(["COMMON.OK", "COMMON.CANCEL"]).then(function(translations){
        __Notificationmanager.prototype.okLabel = translations['COMMON.OK'];
        __Notificationmanager.prototype.cancelLabel = translations['COMMON.CANCEL'];
    });
};

__Notificationmanager.prototype.execute = function(pmsg, pdata, event) {
    if (typeof pdata != 'undefined') {
        pdata.title = sanitizeNotificationText(pdata.title);
        pdata.message = sanitizeNotificationText(pdata.message);
        if(pdata.data) {
            pdata.data.sanitize = true;
        }
    }
    if (pmsg === "showModalDialog") {
        this.showModalDialog({
            "title": pdata.title,
            "message": pdata.message,
            "translate": pdata.translate,
            "onOk": function () {
                if (event.source) {
                    event.source.postMessage(["notificationConfirmed", ""], "*");
                } else {
                    console.warn("Source of confirmation popup not existing anymore, ignoring confirmation.");
                }
            },
            "onCancel": function () {
                if (event.source) {
                    event.source.postMessage(["notificationCancelled", ""], "*");
                } else {
                    console.warn("Source of confirmation popup not existing anymore, ignoring confirmation.");
                }
            },
            "fullBlocking": pdata.fullBlocking,
            "data": pdata.data
        });
    } else if(pmsg === 'showSuccess' || pmsg === 'showError' || pmsg === 'showWarning' || pmsg === 'showInfo') {
        this[pmsg](pdata.message, pdata.data);
    } else {
        this[pmsg](pdata);
    }
};

var sanitizeNotificationText = function(text) {
    return String(text).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace('&lt;br&gt;','<br>');
};

var isValidUrl = function(data) {
    return data.match(/^([A-Za-z0-9\-\._~:\/\?#[\]@!\$&'()*\+,;=%]+)$/);
}

var preProcessNotificationExtraData = function(data, translations) {
    if(data === undefined || data === null) {
        return {};
    }

    var processedData = {};
    for(var key in data) {
        var content = data[key];
        if(typeof content === 'object') {
            if(content.type === 'link') {
                var text = content.text;
                if(translations !== undefined) {
                    text = translations[text];
                }
                if(data.sanitize) {
                    text = sanitizeNotificationText(text);
                }
                processedData[key] = "&nbsp;";
                if(content.external) {
                    if(data.sanitize) {
                        console.log("External URL " + content.url + " not allowed for builder modules.");
                    } else if(!isValidUrl(content.url)) {
                        console.log("External URL " + content.url + " contains invalid character(s).");
                    }
                    else {
                        processedData[key] = '<a href="' + content.url + '"';
                        if(content.target === "_blank") {
                            processedData[key] += ' target="_blank"';
                        }
                        processedData[key] += '>';
                        processedData[key] += text;
                        processedData[key] += '</a>';
                    }
                } else {
                    processedData[key] = '<a href="" onclick="';
                    processedData[key] += "window._linkManager().execute('open', {link: '" +  encodeURIComponent(content.url) + "', urlEncoded: true, addReferrer: false, addToHistory: false}); return false;";
                    processedData[key] += '">';
                    processedData[key] += text;
                    processedData[key] += '</a>';
                }
                continue;
            }
        }

        processedData[key] = (data.sanitize ? sanitizeNotificationText(content.toString()) : content.toString());
    }

    return processedData;
};

var collectTranslationKeys = function(data) {
    if(data === undefined || data === null) {
        return [];
    }

    var translationKeys = [];
    for(var key in data) {
        var content = data[key];
        if(typeof content === 'object') {
            if(content.type === 'link') {
                translationKeys.push(content.text);
            }
        }
    }

    return translationKeys;
};

var injectExtraData = function(text, data) {
    var re = /\{([a-zA-Z0-9]+)\}/g;
    var m;
    var replacementInfos = [];
    do {
        m = re.exec(text);
        if (m) {
            replacementInfos.push(
                {token: m[0],
                    tokenName: m[1]});
        }
    } while (m);

    for(var i in replacementInfos) {
        var info = replacementInfos[i];
        text = text.replace(info.token, function myFunction(x) {
            if(data[info.tokenName]) {
                return data[info.tokenName];
            } else {
                return x;
            }
        });
    }
    return text;
};


__Notificationmanager.prototype.pushProcessing = function () {
    $(".userActionBlockingCnt").show();
    this.counter++;
    var that = this;
    this.ng_timeout(function () {
         $(".userActionBlockingCnt").hide();
         if(that.counter > 0) {
             $(".processingCnt").fadeIn("fast");
         }
   }, 500);
};
__Notificationmanager.prototype.popProcessing = function () {
    if(this.counter > 0) {
        this.counter--;
    }
    if (this.counter == 0) {
        $(".processingCnt").fadeOut( "fast");
    }
};
__Notificationmanager.prototype.clearProcessing = function () {
    this.counter = 0;
    $(".processingCnt").fadeOut( "fast");
};

__Notificationmanager.prototype.clearConfirmation = function () {
    $('#globalModalDialog').modal("hide");
    $('#globalModalDialogBackdrop').hide();
};

var isDuplicateOfPrevious = function(incomingNotification, allNotifications) {
    if (_.isEmpty(allNotifications)) {
        return false;
    }
    else {
        var messagesEqual = _.last(allNotifications).message === incomingNotification.message;
        var detailsEqual = _.isEqual(_.last(allNotifications).details, incomingNotification.details);
        return messagesEqual && detailsEqual;
    }
};

__Notificationmanager.prototype.addNotification = function(notification) {
    var that = this;
    that.ng_timeout(function(){
        notification.created=new Date();
        /* pre-process notification */
        if(!notification.message) {
            return;
        }
        var msgParts = notification.message.split("((msgdetails))");
        notification.message = msgParts[0];
        if(msgParts.length > 1) {
            notification.details = msgParts[1].split("<br>");
        }
        if(notification.data) {
            var data = preProcessNotificationExtraData(notification.data);
            notification.message = injectExtraData(notification.message, data);
            for(var i in notification.details) {
                notification.details[i] = injectExtraData(notification.details[i], data);
            }
        }

        if (!isDuplicateOfPrevious(notification, that.scope.notifications)) {
            that.scope.notifications.push(notification);
        }

        if(notification.level === "alert-success") {
            notification.timeOutPromise = that.ng_timeout(function () {
                that.removeNotification(notification);
            }, that.notificationTimeout);
        } else {
            notification.timeoutAborted = true;
        }
    });
};

__Notificationmanager.prototype.removeNotification = function(notification) {
    var index = this.scope.notifications.indexOf(notification);
    this.scope.notifications.splice(index, 1);
};

__Notificationmanager.prototype.removeTimeout = function(notification) {
    this.ng_timeout.cancel(notification.timeOutPromise);
};

__Notificationmanager.prototype.continueTimeout = function(notification) {
   if(!notification.timeoutAborted) {
       var that = this;
       notification.timeOutPromise = this.ng_timeout(function() {
           that.removeNotification(notification);
       }, this.notificationTimeout);
   }
};

__Notificationmanager.prototype.showSuccess = function(msg, data) {
   this.addNotification({
       level: "alert-success",
       message: msg,
       data: data
   });
};
__Notificationmanager.prototype.showError = function(msg, error, data) {
   this.addNotification({
       level: "alert-danger",
       message: msg,
       error: error,
       data: data
   });
};
__Notificationmanager.prototype.showWarning = function(msg, data) {
   this.addNotification({
       level: "alert-warning",
       message: msg,
       data: data
   });
};
__Notificationmanager.prototype.showInfo = function(msg, data) {
   this.addNotification({
       level: "alert-info",
       message: msg,
       data: data
   });
};
__Notificationmanager.prototype.getErrorMessage = function(errorResponse) {
    var msg = "";
    var error = errorResponse.data;
    if(Object.prototype.toString.call(error.details) === "[object Array]" && error.details.length > 0) {
        msg = "((msgdetails))";
        for(var i = 0; i< error.details.length; i++) {
            var detailedMsg = error.details[i].message;
            if(detailedMsg !== null && detailedMsg !== undefined && detailedMsg.length > 0) {
                msg += (i!==0?"<br>":"") + detailedMsg;
            }
        }
    } else if(typeof error.message !== 'undefined' && error.message !== null ) {
        msg = error.message;
    } else {
        msg = " " + errorResponse.status + " " + errorResponse.statusText;
    }
    return msg;
};

__Notificationmanager.prototype.showModalDialog = function (modalConfiguration) {

    if(modalConfiguration.translate) {
        var that = this;

        var translationKeys = [];
        translationKeys.push(modalConfiguration.title);
        translationKeys.push(modalConfiguration.message);
        translationKeys = translationKeys.concat(collectTranslationKeys(modalConfiguration.data));

        this.scope.translate(translationKeys).then(function (translations) {
            modalConfiguration.title = translations[modalConfiguration.title];
            modalConfiguration.message = translations[modalConfiguration.message];
            if(modalConfiguration.data) {
                var data = preProcessNotificationExtraData(modalConfiguration.data, translations);
                modalConfiguration.message = injectExtraData(modalConfiguration.message, data);
            }

            that._showModalDialogInternal(modalConfiguration);
        });
    } else {

        if(modalConfiguration.data) {
            var data = preProcessNotificationExtraData(modalConfiguration.data);
            modalConfiguration.message = injectExtraData(modalConfiguration.message, data);
        }

        this._showModalDialogInternal(modalConfiguration);
    }
 };

__Notificationmanager.prototype._showModalDialogInternal = function (modalConfiguration) {
    var that = this;
    if(that.modalDialogVisible === true) {
        that.dialogStack.push(modalConfiguration);
    } else {
        that.modalDialogVisible = true;
        if (modalConfiguration.okLabel == undefined) {
            modalConfiguration.okLabel = this.okLabel;
        }
        if (modalConfiguration.cancelLabel == undefined) {
            modalConfiguration.cancelLabel = this.cancelLabel;
        }
        if (modalConfiguration.closeable == undefined) {
            modalConfiguration.closeable = false;
        }
        if (modalConfiguration.showCancel == undefined) {
            modalConfiguration.showCancel = true;
        }

        that.ng_timeout(function () {
            that.scope.modalConfiguration = modalConfiguration;

            $('#globalModalDialog').toggleClass("modal-dialog-partialblocking", !modalConfiguration.fullBlocking);
            $('#globalModalDialogBackdrop').toggleClass("modal-dialog-partialblocking", !modalConfiguration.fullBlocking);

            $('#globalModalDialog').modal("show");
            $('#globalModalDialogBackdrop').show();

            $('#globalModalDialog').on('hidden.bs.modal', function (e) {
                $('#globalModalDialogBackdrop').hide();
                that.modalDialogVisible = false;
                if(that.dialogStack.length > 0) {
                    that.showModalDialog(that.dialogStack.shift());
                }
            });
        });
    }
};

__Notificationmanager.prototype.showConfirmation = function (title, message, onConfirmCallback, onCancelCallback, translate, data) {

    this.showModalDialog({
        "message": message,
        "title": title,
        "translate": translate,
        "onOk": onConfirmCallback,
        "onCancel" : onCancelCallback?onCancelCallback:function(){},
        "fullBlocking" : false,
        "okLabel" : this.okLabel,
        "cancelLabel": this.cancelLabel,
        "closeable": false,
        "data": data
    });
};

__Notificationmanager.prototype.showLocalizedConfirmation = function (title, message, onConfirmCallback, onCancelCallback, data) {
    this.showConfirmation(title, message, onConfirmCallback, onCancelCallback, true, data);
};

__Notificationmanager.prototype.setPageDirty = function (value) {
    this.scope.dirty = value;
    this.scope.dirtyStack[this.scope.viewStackCounter] = value;
};

