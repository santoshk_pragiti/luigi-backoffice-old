'use strict';

/**
 * Constructor. For internal use only.
 *
 * @param ng_scope "$scope" variable of the navigation controller.
 * @param ng_location "$location" variable of the navigation controller.
 * @this {__Linkmanager}
 * @constructor
 */
var __Linkmanager = function(ng_scope, ng_location) {
    this.scope = ng_scope;
    this.location = ng_location;
    this._currentProject = null;
    this._currentTeam = null;
    this._currentLocation = null;
    this._currentLink = null;
    this._currentPath = null;
    this._addReferrerFlag = false;
    this._moduleParam = null;
    this._currentNodePath = null;
};

__Linkmanager.prototype.execute = function(pmsg, pdata) {
    if(pdata.urlEncoded) {
        pdata.link = decodeURIComponent(pdata.link);
    }
    pdata.link = sanitizeLinkManagerLink(pdata.link);

    if(pmsg === "open") {
        this.addReferrer(pdata.addReferrer).path(pdata.link).open(pdata.addToHistory);
    }
    else {
        this[pmsg](pdata);
    }
};

var sanitizeLinkManagerLink = function(link) {
    return link.replace(/[^-A-Za-z0-9 &+@/%=~_|!:,.;\(\)]/g, '');
};

__Linkmanager.prototype._concat = function(link) {
    if(isDefinedNotNull(this._currentLink) && this._currentLink.length > 0) {
        this._currentLink = link + (this._currentLink.charAt(0) === '/' ? '' : '/') + this._currentLink;
    } else {
        this._currentLink = link;
    }
};

__Linkmanager.prototype.goBack = function(data) {
    if(this.hasBack()) {
        this.scope.popView(data);
    } else {
        console.error("No previous page in history.");
    }

};

__Linkmanager.prototype.hasBack = function() {
    return this.scope.viewStackCounter > 1;
};

__Linkmanager.prototype.openFeedback = function() {
    this.scope.openFeedback();
};

/**
 * @private
 */
__Linkmanager.prototype._apply = function() {
    if(isDefinedNotNull(this._currentPath)) {
        this._currentLink = this._currentPath.trim();
    }
    if(isDefinedNotNull(this._currentProject)) {
        this._concat(this._currentProject.path);
    }
    else if(isDefinedNotNull(this._currentTeam)) {
        this._concat(this._currentTeam.path);
    }
    else if(isDefinedNotNull(this._managedOrganizationId)) {
        this._concat(this._managedOrganizationId.path);
    }
    else if(isDefinedNotNull(this._currentLocation)) {
        this._concat(this._currentLocation);
    }
    return this;
};

/**
 * Sets the last part of the link, i.e. the part that comes after
 * currentLocation, currentTeam, currentProject if they are specified.
 *
 * @param {string} link A path seperated by '/'.
 * @this {__Linkmanager}
 * @returns {__Linkmanager} The linkmanager object, i.e. this.
 */
__Linkmanager.prototype.path = function(link) {
    this._currentPath = link;
    return this;
};


/**
 * Sets the first part of the link to the current location, i.e. the path specified by path(...)
 * will be relative to the current location.
 *
 * @this {__Linkmanager}
 * @returns {__Linkmanager} The linkmanager object, i.e. this.
 */
__Linkmanager.prototype.currentLocation = function() {
    this._currentLocation = this._currentNodePath; return this;
};


/**
 * Sets the first part of the link to the current team, i.e. the path specified by path(...)
 * will be relative to the current team node, if it exists.
 *
 * @this {__Linkmanager}
 * @returns {__Linkmanager} The linkmanager object, i.e. this.
 */
__Linkmanager.prototype.currentTeam = function() {
    if (this._cachedContext !== undefined) {
        this._currentTeam = this._cachedContext._currentTeam;
    } else {
        this._currentTeam = this.scope.bindings['_currentTeamId'];
    }
    return this;
};

/**
 * Sets the first part of the link to the current org, i.e. the path specified by path(...)
 * will be relative to the current org node, if it exists.
 *
 * @this {__Linkmanager}
 * @returns {__Linkmanager} The linkmanager object, i.e. this.
 */
__Linkmanager.prototype.currentOrg = function() {
        this._managedOrganizationId = this.scope.bindings['_managedOrganizationId'];
    return this;
};

/**
 * Sets the first part of the link to the current project, i.e. the path specified by path(...)
 * will be relative to the current project node, if it exists.
 *
 * @this {__Linkmanager}
 * @returns {__Linkmanager} The linkmanager object, i.e. this.
 */
__Linkmanager.prototype.currentProject = function() {
    if (this._cachedContext !== undefined) {
        this._currentProject = this._cachedContext._currentProject;
    } else {
        this._currentProject = this.scope.bindings['_currentProjectId'];
    }
    return this;
};

__Linkmanager.prototype.ignoreDirty = function() {
    this._ignoreDirty = true; return this;
};

/**
 * Opens this link.
 *
 * @this {__Linkmanager}
 */
__Linkmanager.prototype.open = function(addToHistory, callback) {
    var that = this;
    var linkCB = function() {
        that._apply();
        if(that._addReferrerFlag) {
            that.location.search("referrer",(that.location.search())['selectedPath']);
        }
        else {
            that.location.search("referrer",null);
        }

        if(that._moduleParam) {
            that.location.search("moduleParam",that._moduleParam);
        }

        that.location.search("pv",addToHistory === true ? '1' : null);

        that.location.search("bck",null);

        that.location.search("selectedPath",that._currentLink);

        if(callback !== undefined) {
            that.scope.viewStackCallbacks[that.scope.viewStackCounter + 1] = callback;
        } else {
            that.scope.viewStackCallbacks[that.scope.viewStackCounter + 1] = null;
        }
        that.scope.$apply();
        that._currentProject = null;
        that._currentTeam = null;
        that._addReferrerFlag = false;
        that._moduleParam = null;
    };
    if(this._ignoreDirty===true) {
        linkCB();
    } else {
        this.scope.checkConfirmNav(function() { setTimeout(linkCB, 1); });
    }
};

/**
 * @this {__Linkmanager}
 * @returns {boolean} true, if this link exists in the navigation, false otherwise.
 */
__Linkmanager.prototype.exists = function() {
    this._apply();
    var path = NodeUtils.splitPath(this._currentLink);
    if(path.length > 0) {
        var node = NodeUtils.getLastNode(path, 0, this.scope.nodes);
        return isDefinedNotNull(node) && node.path === this._currentLink;
    }
    return false;
};

/**
 * Returns a string representation of this link.
 *
 * @this {__Linkmanager}
 * @returns {string} the link as string.
 */
__Linkmanager.prototype.get = function() {
    return this._apply()._currentLink;
};


/**
 * Determines .
 *
 * @this {__Linkmanager}
 * @returns {string} the link as string.
 */
__Linkmanager.prototype.addReferrer = function(addReferrer) {
    this._addReferrerFlag = addReferrer; return this;
};

/**
 * Returns a referrer.
 *
 * @this {__Linkmanager}
 * @returns the reference of the previous location.
 */
__Linkmanager.prototype.getReferrer = function() {
    return (this.location.search())['referrer'];
};

/**
 * Sets custom module parameter
 *
 * @this {__Linkmanager}
 * @returns {__Linkmanager} The linkmanager object, i.e. this.
 */
__Linkmanager.prototype.addModuleParam = function(param) {
    this._moduleParam = param; return this;
};

/**
 * Returns custom module parameter
 *
 * @this {__Linkmanager}
 * @returns the custom module parameter
 */
__Linkmanager.prototype.getModuleParam = function() {
    return (this.location.search())['moduleParam'];
};

/**
 * Reloads the navigation. Use this function to update baas-app
 * after changes that affect the navigation tree
 *
 * @this {__Linkmanager}
 */
__Linkmanager.prototype.refreshNavigation = function(callback) {
    this.scope.fetchTree(callback);
};
