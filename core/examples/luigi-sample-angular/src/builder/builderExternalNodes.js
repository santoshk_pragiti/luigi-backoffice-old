"use strict";

function BuilderExternalNodes() {
    this.ajax = function(cfg) {
        return typeof $ !== "undefined" ? $.ajax(cfg) : BuilderExternalNodes.ajax(cfg);
    };

    this.subscribedPackages = [];
    this.extModulesNodes = [];
    this.extModules = {};
    this.loadedExternalModules = [];


    BuilderExternalNodes.prototype.getProjectExternalNodes = function (orgId, projectId, isTestProject, accessToken, ProjectNodeChildrenProviderCallback) {
        var that = this;

        async.waterfall([
            async.apply(that.getProjectSubscriptions, orgId, projectId, isTestProject, accessToken, that),
            function(subscriptions, orgId, projectId, accessToken, getExternalNodesCallback) {
                that.processSubscriptions(subscriptions, getExternalNodesCallback, orgId, projectId, isTestProject, accessToken);
            }
        ], function(err, result) {
            that.extModulesNodes = that.getExternalModuleNodes(that.extModules);
            ProjectNodeChildrenProviderCallback(that.extModulesNodes);
        });
    };

    BuilderExternalNodes.prototype.processSubscriptions = function(subscriptions, getExternalNodesCallback, orgId, projectId, isTestProject, accessToken) {
        var that=this;

        async.each(subscriptions, function(singleSubscription, subscriptionsCallback) {
            async.waterfall([
                function(callback) {
                    if(that.isValidSubscription(singleSubscription)){
                        that.getProjectSubscriptionPackage(singleSubscription, orgId, projectId, isTestProject, accessToken, callback);
                    }
                    else
                    {
                        callback(null, null);
                    }
                },

                function(subscriptionPackageFull, singleSubscriptionPackageCallback) {
                    if(subscriptionPackageFull) {
                        that.subscribedPackages.push(subscriptionPackageFull.id);
                    }
                    that.processSingleSubscriptionPackage(subscriptionPackageFull, singleSubscriptionPackageCallback);
                }
            ], function(err, result) {
                subscriptionsCallback(null);
            });
        }, function(err) {
            getExternalNodesCallback(null);
        });
    };

    BuilderExternalNodes.prototype.isValidSubscription = function(subscription){
        return  subscription.validUntil === undefined || subscription.validUntil === null || new Date(subscription.validUntil) >= new Date();
    };

    BuilderExternalNodes.prototype.processSingleSubscriptionPackage = function(subscriptionFull, singleSubscriptionCallback) {
        var that=this;
        var aPackage = subscriptionFull;
        if (aPackage != null) {
            var uiModules = aPackage.uiModules;
            async.each(uiModules, function processUiModule(uiModule, allUiModulesProcessedCallback) {
                if (uiModule != null) {
                    var moduleUrl = uiModule.moduleUrl;
                    var client_id = uiModule.clientId;
                    var module_id = uiModule.id;
                    var requiredScopes;
                    if (uiModule.requiredScopes !== undefined) {
                        requiredScopes = uiModule.requiredScopes;
                    }
                    else {
                        requiredScopes = [];
                    }
                    async.waterfall([
                        function(callback) {
                            that.getOrLoadExternalModule(uiModule, callback);
                        },
                        function(module, uiModule, moduleProcessedCallback) {
                            that.addPropertiesToProcessedModule(module, uiModule, moduleProcessedCallback)
                        }
                    ], function (err, result) {
                        if (err !== null) {
                            console.error(err);
                        }
                        allUiModulesProcessedCallback();
                    });
                }
            }, function (err) {
                singleSubscriptionCallback(null);
            });
        } else {
            singleSubscriptionCallback(null);
        }
    };

    BuilderExternalNodes.prototype.addPropertiesToProcessedModule = function(module, uiModule, moduleProcessedCallback) { //do we need both module and uiModule?
        var that = this;
        if (module !== null && module !== undefined) {
            module._client_id = uiModule.clientId;
            module._module_id = uiModule.id;
            module._redirectUris = uiModule.redirectUris;
            module._required_scopes = this.scopeArrayToString(uiModule.requiredScopes);
            that.extModules[uiModule.moduleUrl] = module;
        }
        moduleProcessedCallback(null);
    };

    BuilderExternalNodes.prototype.getProjectSubscriptions = function (orgId, projectId, isTestProject, accessToken, thisObject, callback) {
        thisObject.ajax({
            url: _globalSettings.subscriptionServiceUrl + "/subscriberOrgs/" + orgId + "/subscribers/" + projectId + "/subscriptions",
            headers: {'Authorization': 'Bearer ' + accessToken},
            type: 'GET',
            data:{pageSize:999},
            success: function (data) {
                callback(null, data, orgId, projectId, accessToken);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
                callback(null, [], orgId, projectId, accessToken);
            }
        });
    };

    BuilderExternalNodes.prototype.getProjectSubscriptionPackage = function (subscription, orgId, projectId, isTestProject, accessToken, callback) {

        this.ajax({
            url: _globalSettings.marketplaceServiceUrl + "/packages/" + subscription.package.id + "/" + subscription.package.version + "?subscriptionId=" + subscription.id,
            headers: {'Authorization': 'Bearer ' + accessToken},
            type: 'GET',
            success: function (data) {
                var pkg = null;
                if (data.length > 0) {
                    pkg = data[0];
                    pkg.uiModules = pkg.mixins.package.builderModules;
                } else {
                    console.error("subscribed package with ID '" + subscription.package.id + "' and version '" + subscription.package.version + "' can not be fetched for subscription with ID '" + subscription.id + "'");
                }
                callback(null, pkg);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
                callback(null, null);
            }
        });
    };

    BuilderExternalNodes.prototype.scopeArrayToString = function(requiredScopes) {
        if (requiredScopes === undefined) {
            return undefined;
        }
        var requiredScopesToString = "";
        for (var i = 0; i < requiredScopes.length; i++) {
            if (i > 0) {
                requiredScopesToString += " ";
            }
            requiredScopesToString += requiredScopes[i];
        }
        return requiredScopesToString;
    };

    BuilderExternalNodes.prototype.isUsingSecuredProtocol = function(url) {
        var httpsUsed = url.substring(0, 5) === 'https';
        if (!httpsUsed)
        {
            console.error('Builder module ' + url + ' cannot be used via unsecured connection! Use HTTPS!');
        }
        return httpsUsed;
    };

    BuilderExternalNodes.prototype.getOrLoadExternalModule = function(uiModule, callback) {
        var loadedModules = this.getLoadedExternalModules();

        var extModuleUrl = uiModule.moduleUrl;

        if (!loadedModules[extModuleUrl] !== undefined && this.isUsingSecuredProtocol(extModuleUrl)) {
            this.loadExternalModule(extModuleUrl, function(extModule) {
                loadedModules[extModuleUrl] = extModule;
                callback(null, loadedModules[extModuleUrl], uiModule);
            });
        }
        else {
            callback('Error loading Builder Module at: ' + extModuleUrl);
        }
    };

    BuilderExternalNodes.prototype.loadExternalModule = function (extModuleUrl, callback) {
        var that = this;
        that.ajax({
            url: extModuleUrl,
            type: 'GET',
            dataType: "json",
            success: function(data) {
                callback(data);
            },
            error: function() {
                console.log("Builder Module "+extModuleUrl+" is not reachable!");
                callback(undefined);
            }
        });
    };

    BuilderExternalNodes.prototype.getLoadedExternalModules = function () {
        return this.loadedExternalModules;
    };

    BuilderExternalNodes.prototype.getExternalModuleNodes = function (externalModules) {
        var externalModuleNodes = [];
        var domainMap = [];
        for (var key in externalModules) {
            var moduleInfo = externalModules[key];

            var moduleUrl = key.replace("rest/cockpitng/module", "")
                .replace("baas/module.xml", "").replace("baas/module.json", "")
                .replace(/builder\/module[a-zA-Z0-9_-]*\.json$/, "");

            if (/\/$/.test(moduleUrl)) {
                moduleUrl = moduleUrl.substring(0, moduleUrl.length - 1);
            }
            var widgetsArray = [];
            var widgets = moduleInfo.widgets;
            if (widgets instanceof Array) {
                widgetsArray = widgets;
            }
            else {
                errors.push("widgets entry of external module is not an array! url : " + moduleUrl);
                continue;
            }
            var remoteNavigation = false;
            if (moduleInfo.remoteNavigation !== undefined && moduleInfo.remoteNavigation == true) {
                remoteNavigation = true;
            }

            var translations = moduleInfo.translations;
            if (translations == undefined) {
                translations = {};
            }
            var filteredWidgetArray = [];
            for (var index = 0; index < widgetsArray.length; index++) {
                var element = widgetsArray[index];
                if (this.isVisible(element)) {
                    filteredWidgetArray.push(element);
                }
            }

            var modInfo = {};
            modInfo._client_id = moduleInfo._client_id;
            modInfo._module_id = moduleInfo._module_id;
            modInfo._required_scopes = moduleInfo._required_scopes;
            modInfo._redirectUris = moduleInfo._redirectUris;
            modInfo._module_id  = moduleInfo._module_id;
            if(this.subscribedPackages.length > 0) {
                modInfo._packages = this.subscribedPackages;
            }
            modInfo.translations = translations;
            var tourStep = this.getTourStep(moduleInfo);
            if (tourStep) {
                modInfo.tourStep = tourStep;
            }

            for (var i = 0; i < filteredWidgetArray.length; i++) {
                var widget = filteredWidgetArray[i];
                this.processWidget(widget, modInfo, moduleUrl, null, remoteNavigation, externalModuleNodes, domainMap, i);
            }
        }
        return externalModuleNodes.sort(function(a,b) {
            if(a.sortingKey && b.sortingKey) {
                return a.sortingKey.localeCompare(b.sortingKey);
            } else if(a.sortingKey) {
                return 1; // a is 'less'
            } else if(b.sortingKey) {
                return -1;
            } else {
                return 0;
            }
        });
    };

    var containsSameNode = function(externalModuleNodes, externalNode) {
        for(var n in externalModuleNodes) {
            var node = externalModuleNodes[n];
            if(node.id == externalNode.id && node.extUrl == externalNode.extUrl && node.domain !== true) {
                return true;
            }
        }
        return false;
    };

    BuilderExternalNodes.prototype.processWidget = function (widget, moduleInfo, moduleUrl, parentWidget,
        remoteNavigation, externalModuleNodes, domainMap, index) {
        var added = false;
        var title = widget.title;
        var id = widget.id !== undefined ? widget.id : title; // backward compatibility

        var settings = this.getWidgetSettings(widget);

        var viewUrl = "";
        var requiresServices = null;
        var perspectiveDomain = null;

        var customSettings = {};

        for (var key in settings) {
            var value = settings[key];
            if ("viewUrl".toLowerCase() === key.toLowerCase()) {
                viewUrl = value;
            }
            else if ("perspectiveDomain".toLowerCase() === key.toLowerCase()) {
                perspectiveDomain = value;
            }
            else if ("requiresServices".toLowerCase() === key.toLowerCase()) {
                requiresServices = value;
            }
            else {
                customSettings[key] = value;
            }
        }

        if (moduleInfo._client_id !== undefined) {
            customSettings._client_id = moduleInfo._client_id;
        }
        if (moduleInfo._module_id !== undefined) {
            customSettings._module_id = moduleInfo._module_id;
        }
        if (moduleInfo._required_scopes !== undefined) {
            customSettings._required_scopes = moduleInfo._required_scopes;
        }

        if (/^\//.test(viewUrl) || /^\\/.test(viewUrl)) {
            viewUrl = viewUrl.substring(1);
        }

        var externalNode = {
            id: id,
            label: title,
            settings: customSettings,
            moduleInfo: moduleInfo,
            translations: moduleInfo.translations
        };

        if(isDefinedNotNull(viewUrl) && viewUrl.trim().length > 0) {
            externalNode.extUrl = moduleUrl + "/" + viewUrl;
        }

        if (moduleInfo._redirectUris !== undefined) {
            externalNode._redirectUris = moduleInfo._redirectUris;
        }

        if (requiresServices !== null) {
            externalNode.requiresServices = requiresServices;
        }

        if (widget.virtualChildren !== undefined) {
            externalNode.virtualChildren = widget.virtualChildren;
        }
        if (widget.dynamic !== undefined) {
            externalNode.dynamic = widget.dynamic;
        }
        if (widget.initialSelect !== undefined) {
            externalNode.initialSelect = widget.initialSelect;
        }
        if (remoteNavigation) {
            externalNode.remoteNavigation = remoteNavigation;
        }
        if (widget.extensible !== undefined) {
            externalNode.extensible = widget.extensible;
        }
        if (widget.extends !== undefined) {
            externalNode.extends = widget.extends;
            externalNode.moduleBasePath = moduleUrl;
        }

        var tourStep = this.getTourStep(widget);
        if (tourStep) {
            externalNode.tourStep = tourStep;
        }

        if(widget.sortingKey !== undefined) {
            externalNode.sortingKey = widget.sortingKey;
        } else if(index !== undefined) {
            var indString = '' + (index + 1);
            var nrZeros = 5 - indString.length;
            for(var i = 0; i <= nrZeros; i++) {
                indString = "0" + indString;
            }
            externalNode.sortingKey = moduleInfo._module_id + indString + "00";
        }

        if (parentWidget != undefined) {
            if (parentWidget.nodes === undefined) {
                parentWidget.nodes = [];
            }
            if(!containsSameNode(parentWidget.nodes, externalNode)) {
                parentWidget.nodes.push(externalNode);
                added = true;
            }
        }
        else {
            if (perspectiveDomain !== null) {
                var domainNodes = domainMap[perspectiveDomain];
                if (domainNodes == null) {
                    domainNodes = [];
                    domainMap[perspectiveDomain] = domainNodes;
                    var domain = {};
                    domain.nodes = domainNodes;
                    domain.id = perspectiveDomain;
                    domain.domain = true;
                    domain.settings = customSettings;
                    domain.moduleInfo = moduleInfo;
                    domain.translations = moduleInfo.translations;
                    externalModuleNodes.push(domain);
                }
                if(!containsSameNode(domainNodes, externalNode)) {
                    domainNodes.push(externalNode);
                    added = true;
                }
            } else {
                if(!containsSameNode(externalModuleNodes, externalNode)) {
                    externalModuleNodes.push(externalNode);
                    added = true;
                }
            }
        }

        if (added && widget.widgets !== undefined) {
            var subNodes = widget.widgets;
            if(subNodes.length !== undefined) {
                for (var i = 0; i < subNodes.length; i++) {
                    if (this.isVisible(subNodes[i])) {
                        this.processWidget(subNodes[i], moduleInfo, moduleUrl, externalNode,
                            remoteNavigation, externalModuleNodes, domainMap, i);
                    }
                }
            } else {
                console.error("Error getting subnodes of '"+ id +"', property 'widgets' seems to be not an array.");
            }
        }
    };


    BuilderExternalNodes.prototype.isVisible = function (element) {
        if (element.scopes && element.scopes.length > 0) {
            for (var scope_i = 0; scope_i < element.scopes.length; scope_i++) {
                var scope = element.scopes[scope_i];
                if (window._authManager().hasScope(scope, true)) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }


    BuilderExternalNodes.prototype.getWidgetSettings = function (widget) {
        var ret = [];

        if (widget.setting !== undefined) {
            for (var key in widget.setting) {
                ret[key] = widget.setting[key];
            }
        }
        else if (widget.settings !== undefined) {
            for (var key in widget.settings) {
                ret[key] = widget.settings[key];
            }
        }
        return ret;
    }

    BuilderExternalNodes.prototype.getTourStep = function (widget) {
        if(widget.tourStep) {
            return {
                "heading": widget.tourStep.heading,
                "text": widget.tourStep.text
            }
        }
        return undefined;
    }
}
