var _globalSettings = {
	"accountServiceUrl" : "https://api.yaas.io/hybris/account/v1",
	"authorizeUrl" : "https://api.yaas.io/hybris/oauth2/v1/authorize",
	"baasLoginUrl" : "/login/login.html",
	"betaFeaturesEnabled" : "false",
	"billingServiceUrl" : "https://api.yaas.io/sap/bill/v1",
	"builderAppId" : "hybris.builder",
	"clientId" : "Bin5mve9QLuEpYggjTeMzjQoklXr4aav",
	"coinServiceUrl" : "https://api.yaas.io/hybris/coin-showcase/v1",
	"coinportalUrl" : "https://community.yaas.io/",
	"configurationServiceUrl" : "https://api.yaas.io/hybris/configuration/v1",
	"context_switch_redirect_uri" : "https://dev.local:8443/auth/callback.html",
	"delegoRemoteDomain" : "https://sap-isp-public.delego-cloud.com",
	"devportalSignup" : "https://devportal.yaas.io/signup.html",
	"devportalUrl" : "https://devportal.yaas.io",
	"expertForumUrl" : "https://experts.hybris.com/spaces/151/index.html",
	"feedbackServiceUrl" : "https://api.yaas.io/hybris/feedback/v1",
	"gaTrackingId" : "UA-56768310-7",
	"jiraUrl" : "https://jira.hybris.com/browse/CFB",
	"marketplaceServiceUrl" : "https://api.yaas.io/hybris/marketplace/v3",
	"marketplaceUrl" : "https://market.yaas.io",
	"mediaRepositoryServiceUrl" : "https://api.yaas.io/hybris/media/v1",
	"oauthServiceUrl" : "https://api.yaas.io/hybris/oauth2/v1",
	"organizationMediaServiceUrl" : "https://api.yaas.io/hybris/org-media/v1",
	"redirect_uri" : "https://dev.local:8443",
	"resultServiceUrl" : "https://api.yaas.io/hybris/result-service/v0",
	"scope" : "FULL_ACCESS hybris.configuration_manage hybris.account_manage hybris.account_view hybris.marketplace_submit hybris.media_manage hybris.org_manage hybris.org_view sap.bill_view sap.subscription_manage hybris.org_payment hybris.org_project_create hybris.org_members sap.subscription_cancel sap.subscription_provider_view hybris.showcase_manage",
	"skipSslVerification" : "true",
	"ssoWhitelist" : "https://www.yaas.io,https://dev.local:8443,https://market.yaas.io,https://devportal.yaas.io,https://techne.yaas.io,https://community.yaas.io",
	"store_link" : "https://market.yaas.io",
	"subscriptionServiceUrl" : "https://api.yaas.io/sap/subscription/v1",
	"userInfoServiceUrl" : "https://api.yaas.io/hybris/userinfo/v1",
	"version" : "1.5.0-SNAPSHOT",
	"yaasSignup" : "https://www.yaas.io/register",
	"yaas_home" : "http://www.yaas.io/home",
	"yaas_proxy_url" : "https://api.yaas.io"
};