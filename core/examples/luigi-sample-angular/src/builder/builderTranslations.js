var translatePresent = true;
try {
    angular.module('pascalprecht.translate');
} catch (err) {
    translatePresent = false;
}
var translationsModule;
if(translatePresent) {
    translationsModule = angular.module('builder.translations', ['pascalprecht.translate']);
    translationsModule.constant('availableLanguages',
        ['en', 'de']
    );
    translationsModule.constant('languageMappings',
        {
            'en_US': 'en',
            'en_UK': 'en',
            'de_DE': 'de',
            'de_CH': 'de',
            '*': 'en'
        }
    );
    translationsModule.constant('fallbackLanguage',
        'en'
    );
    translationsModule.config(['$translateProvider', 'availableLanguages', 'languageMappings', 'fallbackLanguage',
        function ($translateProvider, availableLanguages, languageMappings, fallbackLanguage) {
        $translateProvider
            .useStaticFilesLoader({
                prefix: '/public/locales/locale_',
                suffix: '.json?v=' + (window.Builder ? window.Builder.globalSettings.version : window._globalSettings.version)
            })
            .useStorage('currentLanguageStorage')
            .registerAvailableLanguageKeys(availableLanguages, languageMappings)
            .fallbackLanguage(fallbackLanguage);
            var curr = localStorage.getItem('builder.language.current');
            if (curr != undefined || curr != null) {
                $translateProvider.preferredLanguage(curr);
                $translateProvider.use(curr);
            } else {
                curr = localStorage.getItem($translateProvider.storageKey());
                if (curr != undefined || curr != null) {
                    localStorage.setItem('builder.language.current', curr);
                } else {
                    $translateProvider.determinePreferredLanguage();
                }
            }
    }]);
    translationsModule.service('currentLanguage', function($translate) {
        return {
            set: function (lang) {
                if(lang) {
                    localStorage.setItem('builder.language.current', lang);
                    return $translate.use(lang);
                } else {
                    var curr = $translate.use();
                    if (curr == undefined || curr == null) {
                        curr = $translate.preferredLanguage();
                    }
                    localStorage.setItem('builder.language.current', curr);
                    return $translate.use(curr);
                }
            },
            get: function () {
                var curr = localStorage.getItem('builder.language.current');
                if (curr == undefined || curr == null) {
                    curr = $translate.use();
                }
                if (curr == undefined || curr == null) {
                    curr = $translate.preferredLanguage();
                }
                return curr;
            }
        };
    });
    translationsModule.factory('currentLanguageStorage', function () {
        return {
            put: function (name, value) {
                localStorage.setItem(name, value);
            },
            get: function (name) {
                return localStorage.getItem(name);
            }
        };
    });
    translationsModule.controller('translationsCtrl', function ($scope) {
    });
}


