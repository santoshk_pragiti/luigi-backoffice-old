"use strict";

var __Authmanager = function() {};

__Authmanager.prototype.getCurrentUser = function(){
    return localStorage.getItem("_currentUser");
};
__Authmanager.prototype.setCurrentUser = function(user){
    return localStorage.setItem("_currentUser", user);
};

__Authmanager.prototype.getScope = function(){
    return localStorage.getItem("_scope");
};
__Authmanager.prototype.setScope = function(scope){
    return localStorage.setItem("_scope", scope);
};
__Authmanager.prototype.getAccessToken = function(){
    return localStorage.getItem("_accessToken");
};
__Authmanager.prototype.setAccessToken = function(token){
    return localStorage.setItem("_accessToken", token);
};

//TSK-3162
__Authmanager.prototype.setCurrentAccountId = function(username){
    return localStorage.setItem("_currentAccountId", username);
};
__Authmanager.prototype.getCurrentAccountId = function(){
    return localStorage.getItem("_currentAccountId");
};
__Authmanager.prototype.setCurrentModuleClientId = function(clientId){
    return localStorage.setItem("_currentModuleId",clientId);
};
__Authmanager.prototype.getCurrentModuleClientId = function(){
    return localStorage.getItem("_currentModuleId");
};
__Authmanager.prototype.setCurrentProjectId = function(tenant){
    return localStorage.setItem("_currentProjectId",tenant);
};
__Authmanager.prototype.getCurrentProjectId = function(){
    return localStorage.getItem("_currentProjectId");
};


__Authmanager.prototype.handleMissingToken = function (state) {
    return;
};

__Authmanager.prototype.consumeAuthentication = function () {
    return;
};

__Authmanager.prototype.hasToken = function () {
    return true;
};

__Authmanager.prototype.switchTenant = function(tenant, state){
    return;
};
__Authmanager.prototype.updateSSO = function() {
    return;
};

__Authmanager.prototype.hasScope = function(scope) {
    var scopes = this.getScope();
    if(scopes) {
        var scopeArray = decodeURI(scopes).split(" ");
        if(scopeArray.length > 0) {
            return scopeArray.indexOf(scope) >= 0;
        }
    }
    return false;
};


(function () {
    var am = new __Authmanager();
    if (am.getCurrentUser() === undefined || am.getCurrentUser() === null || am.getCurrentUser().trim().length === 0) {
        am.setScope("test_scope");
        am.setCurrentUser("test_user");
        am.setAccessToken("0000000000000");
        localStorage.removeItem("builder_clientId");
    }
})();

$(document).ready(function () {
    $(".toggle-dev-btn").click(function (event) {
        if ($(document.body).hasClass("dev-close")) {
            $(document.body).removeClass("dev-close");
            sessionStorage.setItem('baasDevClose', 'false');
        } else {
            $(document.body).addClass("dev-close");
            sessionStorage.setItem('baasDevClose', 'true');
        }
    });
    if (sessionStorage.getItem('baasDevClose') === 'true') {
        $(document.body).addClass("dev-close");
    }

    if(localStorage.getItem("hasTimestampChanged")==="true"){
        var am = new __Authmanager();
        localStorage.setItem("dev-currentProjectId", am.getCurrentProjectId());
        localStorage.setItem("dev-currentAccountId", am.getCurrentAccountId());
        localStorage.setItem("dev-currentModuleId", am.getCurrentModuleClientId());

        window._currentProjectId = am.getCurrentProjectId();
        window._currentAccountId = am.getCurrentAccountId();
        window._currentLanguage = localStorage.getItem("dev-currentLanguage");
        window._moduleId = am.getCurrentModuleClientId();

    }else {
        window._currentProjectId = localStorage.getItem("dev-currentProjectId");
        window._currentAccountId = localStorage.getItem("dev-currentAccountId");
        window._currentLanguage = localStorage.getItem("dev-currentLanguage");
        window._moduleId = localStorage.getItem("dev-currentModuleId");
    }
});

angular.module("baasApp").controller("DevCtrl", function ($scope, $timeout, $interval, $location, $http, $translate, currentLanguage) {
    $scope.localSettings = {};
    $scope.editMode = false;
    $scope.localSettingKeys = {};
    $scope.devModules = [];
    $scope.currentWidgetSettings = [];
    $scope.timeleft = "";

    $interval(function() {
        if(window._expDate) {
            var millisLeft = window._expDate - new Date().getTime();
            if(millisLeft > 0) {
                var secsLeft = Math.round(millisLeft/1000);
                var secFract = secsLeft%60;
                $scope.timeleft = Math.floor(secsLeft/60) + ":" + (secFract < 10 ? "0" : "") + secFract;
            } else {
                $scope.timeleft = "expired";
            }
        } else {
            $scope.timeleft = "expired";
        }
    }, 1000);

    window._currentProjectLocales = (sessionStorage.getItem("dev-currentLocales") === null ? {
        "languages":[{"id":"en","label":"English","default":true,"required":true}],
        "currencies":[{"id":"USD","label":"US Dollar","default":true,"required":true}]} : JSON.parse(sessionStorage.getItem("dev-currentLocales")));

    $scope.loadSettings = function () {
        var ls = $scope.localSettings;
        var am = new __Authmanager();
        ls.currentAccountId = window._currentAccountId;
        ls.currentLanguage = window._currentLanguage;
        ls.currentTeamId = window._currentTeamId;
        ls.currentProjectId = window._currentProjectId;
        ls.yaasClient = window._moduleId;
        ls.authScope = am.getScope();
        ls.accessToken = am.getAccessToken();
        ls.currentProjectLocales = JSON.stringify(window._currentProjectLocales);
        ls.clientId = window._moduleId;


        loadCommonSettings();
    };

    $scope.reloadSettings = function () {
        window._getToken(true);
        var ls = $scope.localSettings;
        var am = new __Authmanager();
        ls.currentAccountId = am.getCurrentAccountId();
        ls.currentLanguage = window._currentLanguage;
        ls.currentTeamId = window._currentTeamId;
        ls.currentProjectId = am.getCurrentProjectId();
        ls.yaasClient = am.getCurrentModuleClientId();
        ls.authScope = am.getScope();
        ls.accessToken = am.getAccessToken();
        ls.currentProjectLocales = JSON.stringify(window._currentProjectLocales);
        ls.clientId = am.getCurrentModuleClientId();


        loadCommonSettings();
        $scope.saveSettings();
    };

    var loadCommonSettings = function(){
        $scope.localSettingKeys.additionalModuleInfo = ["yaasClient"];
        $scope.localSettingKeys.account = ["currentAccountId", "currentLanguage"];
        $scope.localSettingKeys.auth = ["authScope", "accessToken", "clientId"];
        $scope.localSettingKeys.project = ["currentProjectId", "currentProjectLocales"];


        var cnode = $scope.getExampleProjectNode();
        var moduleViews = cnode.nodes.slice(1, cnode.nodes.length - 1);
        $scope.devModules = moduleViews;

        $scope.selectedModule = $scope.devModules[0];
        $scope.localSettingKeys.module = ["moduleNodes"];
        $scope.currentWidgetSettings = $scope.getWidgetSettings($scope.selectedModule);


    }

    $timeout($scope.loadSettings, 1000);

    $scope.selectModule = function (mod) {
        $scope.selectedModule = mod;
        $scope.currentWidgetSettings = $scope.getWidgetSettings($scope.selectedModule);
    };

    $scope.getWidgetSettings = function (node) {
        var settings = [];
        for (var setting in node.settings) {
            settings.push({
                key: setting,
                value: node.settings[setting]
            });
        }
        return settings;
    };

    $scope.openCurrentModule = function () {
        window.location.hash = "?selectedPath=" + $scope.selectedModule.path;
    };

    $scope.refreshPage = function(){
        var index = window.location.hash.indexOf("&r=1");
        if(index > -1) {
            window.location.hash = window.location.hash.slice(0, index);
        }else{
            window.location.hash += "&r=1";
        }
    };

    $scope.enableEdit = function () {
        $scope.editMode = true;
    };

    $scope.getMainScope = function () {
        return angular.element($(".wrapper").get(0)).scope();
    };

    $scope.getExampleProjectNode = function () {
        return $scope.getMainScope().nodes[0].nodes[0].nodes[0];
    }

    $scope.saveSettings = function () {
        $scope.editMode = false;
        angular.element($(".wrapper").get(0)).scope();
        var ls = $scope.localSettings;
        window._currentAccountId = ls.currentAccountId;
        window._currentTeamId = ls.currentTeamId;
        window._currentProjectId = ls.currentProjectId;
        window._currentSavedProjectId = ls.currentProjectId;
        window._currentProjectLocales = JSON.parse(ls.currentProjectLocales);
        window._currentLanguage = ls.currentLanguage;
        window._moduleId = ls.yaasClient;
        var am = new __Authmanager();
        am.setScope(ls.authScope);
        am.setCurrentUser(ls.currentAccountId);
        am.setAccessToken(ls.accessToken);
        var mainScope = $scope.getMainScope();
        mainScope.currentAccountId = ls.currentAccountId;
        mainScope.currentTeamId = ls.currentTeamId;
        mainScope.currentProjectId = ls.currentProjectId;
        sessionStorage.setItem("dev-currentLocales", JSON.stringify(window._currentProjectLocales));
        localStorage.setItem("dev-currentProjectId", ls.currentProjectId);
        localStorage.setItem("dev-currentAccountId", ls.currentAccountId);
        localStorage.setItem("dev-currentLanguage", ls.currentLanguage);
        localStorage.setItem("dev-currentModuleId", ls.yaasClient);

        if(ls.currentLanguage) {
            currentLanguage.set(ls.currentLanguage).then(function() {
                $scope.reload();
            });
        } else {
            $scope.reload();
        }
    };

    $scope.reload = function() {
        setTimeout(function () {
            var mainScope = $scope.getMainScope();
            mainScope.$apply();
            $scope.loadSettings();
            mainScope.fetchTree();
            /*setTimeout(function () {
             $scope.loadSettings();
             $scope.refreshPage();
             }, 1000);*/
        }, 500);
    };
});


angular.module("baasApp").controller("SigninCtrl", function ($scope, $timeout, $location, $http) {
    $scope.username = localStorage.getItem("dev-currentAccountId");
    $scope.clientId = localStorage.getItem("builder_clientId");
    $scope.tenant = localStorage.getItem("dev-currentProjectId");
});
