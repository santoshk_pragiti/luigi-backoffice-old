var  _globalSettings =  window._globalSettings;

function getAccessToken(){
  return sessionStorage.getItem('temp_access_token');
}
// var nodedefultObject = new Object();
// nodedefultObject.globalSettings = window._globalSettings;
// nodedefultObject.notificationConfig = { autoProcessing: true, autoNotifications: true };
// nodedefultObject.currentWidget = "";
// nodedefultObject.currentWidget.settings = ""
// nodedefultObject.currentAccountId = null;
// nodedefultObject.currentTeamId = null;
// nodedefultObject.cFurrentProjectId = null;
// // if($scope.isDefinedNotNull(nodedefultObject.currentProjectId) && $scope.isDefinedNotNull($scope.bindings['_currentProjectId'])) {
// //     nodedefultObject.currentProjectPath = $scope.bindings['_currentProjectId'].path;
// // }
// // nodedefultObject.currentProjectLocales = window._currentProjectLocales;
// // nodedefultObject.currentLocation = name;
// // nodedefultObject.allNodePaths = $scope.allNodePaths;

// nodedefultObject._referrer = null;
// nodedefultObject.missingDependencies = [];
// // nodedefultObject.currentWidget.settings._client_id = null;
// // nodedefultObject.currentWidget.settings._module_id = null;
// nodedefultObject.helpMode = false;
// nodedefultObject._hasBack = false;
// nodedefultObject.currentLanguage = "en-us";
// nodedefultObject.accessToken = "0000000000000";
// nodedefultObject.scope = "test_scope";
function nodedefultObject(appId) {
  var appIdValue;
  if (appId) {
    appIdValue = appId;
  }else {
    appIdValue = null;
  }
  var nodedefultObjectData = {
    "globalSettings": {
      "accountServiceUrl": "https://api.yaas.io/hybris/account/v1",
      "authorizeUrl": "https://api.yaas.io/hybris/oauth2/v1/authorize",
      "baasLoginUrl": "/login/login.html",
      "betaFeaturesEnabled": "false",
      "billingServiceUrl": "https://api.yaas.io/sap/bill/v1",
      "builderAppId": "hybris.builder",
      "clientId": "Bin5mve9QLuEpYggjTeMzjQoklXr4aav",
      "coinServiceUrl": "https://api.yaas.io/hybris/coin-showcase/v1",
      "coinportalUrl": "https://community.yaas.io/",
      "configurationServiceUrl": "https://api.yaas.io/hybris/configuration/v1",
      "context_switch_redirect_uri": "https://dev.local:8443/auth/callback.html",
      "delegoRemoteDomain": "https://sap-isp-public.delego-cloud.com",
      "devportalSignup": "https://devportal.yaas.io/signup.html",
      "devportalUrl": "https://devportal.yaas.io",
      "expertForumUrl": "https://experts.hybris.com/spaces/151/index.html",
      "feedbackServiceUrl": "https://api.yaas.io/hybris/feedback/v1",
      "gaTrackingId": "UA-56768310-7",
      "jiraUrl": "https://jira.hybris.com/browse/CFB",
      "marketplaceServiceUrl": "https://api.yaas.io/hybris/marketplace/v3",
      "marketplaceUrl": "https://market.yaas.io",
      "mediaRepositoryServiceUrl": "https://api.yaas.io/hybris/media/v1",
      "oauthServiceUrl": "https://api.yaas.io/hybris/oauth2/v1",
      "organizationMediaServiceUrl": "https://api.yaas.io/hybris/org-media/v1",
      "redirect_uri": "https://dev.local:8443",
      "resultServiceUrl": "https://api.yaas.io/hybris/result-service/v0",
      "scope": "FULL_ACCESS hybris.configuration_manage hybris.account_manage hybris.account_view hybris.marketplace_submit hybris.media_manage hybris.org_manage hybris.org_view sap.bill_view sap.subscription_manage hybris.org_payment hybris.org_project_create hybris.org_members sap.subscription_cancel sap.subscription_provider_view hybris.showcase_manage",
      "skipSslVerification": "true",
      "ssoWhitelist": "https://www.yaas.io,https://dev.local:8443,https://market.yaas.io,https://devportal.yaas.io,https://techne.yaas.io,https://community.yaas.io",
      "store_link": "https://market.yaas.io",
      "subscriptionServiceUrl": "https://api.yaas.io/sap/subscription/v1",
      "userInfoServiceUrl": "https://api.yaas.io/hybris/userinfo/v1",
      "version": "1.5.0-SNAPSHOT",
      "yaasSignup": "https://www.yaas.io/register",
      "yaas_home": "http://www.yaas.io/home",
      "yaas_proxy_url": "https://api.yaas.io"
    },
    "notificationConfig": {
      "autoProcessing": true,
      "autoNotifications": true
    },
    "currentWidget": {
      "settings": {
        "appId": appIdValue,
        "_module_id": null
      }
    },
    "currentAccountId": null,
    "currentProjectId": "saasdev2",
    // "currentProjectPath": "/Home/Projects/null",
    "currentProjectLocales": {
      "languages": [
        {
          "id": "en",
          "label": "English",
          "default": true,
          "required": true
        }
      ],
      "currencies": [
        {
          "id": "USD",
          "label": "US Dollar",
          "default": true,
          "required": true
        }
      ]
    },
    // "currentLocation": "/Home/Projects/null/BrandAdmin#1",
    // "allNodePaths": [
    //   "/Home",
    //   "/Home/Projects",
    //   "/Home/Projects/null",
    //   "/Home/Projects/null/Overview",
    //   "/Home/Projects/null/BrandAdmin#1",
    //   "/Home/Projects/null/Administration"
    // ],
    "missingDependencies": [],
    "helpMode": false,
    "_hasBack": false,
    "currentLanguage": "en-us",
    "accessToken": getAccessToken(),
    "scope": "test_scope"
  }
  return nodedefultObjectData;
}

var navigationPermissionChecker = function (nodeToCheckPermissionFor, parentNode, currentContext) {
  // depending on the current path and context returns true or false
  // true means the current node is accessible, false the opposite
  var mockCurrentUserGroups = ['admins'];
  if (nodeToCheckPermissionFor.constraints) {
    // check if user has required groups
    return nodeToCheckPermissionFor.constraints.filter(
      function (c) {
        return mockCurrentUserGroups.indexOf(c) !== -1;
      }
    ).length !== 0;
  }

  return true;
};

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

var getAllProjects = function () {
  return new Promise(function (resolve) {
    resolve([{
        id: 'pr1',
        name: 'Project One'
      },
      {
        id: 'pr2',
        name: 'Project Two'
      }
    ]);
  });
};

var getProjectPlugins = function (projectId) {
  return new Promise(function (resolve) {
    if (projectId === 'pr2') {
      resolve([{
          category: 'ExternalViews',
          viewId: 'viewX',
          label: 'This is X',
          viewUrl: 'https://this.is.x/index.html'
        },
        {
          category: 'ExternalViews',
          viewId: 'viewY',
          label: 'This is Y',
          viewUrl: 'https://this.is.y/index.html'
        }
      ]);
    } else {
      resolve([{
          category: 'ExternalViews',
          viewId: 'abc',
          label: 'A B C',
          viewUrl: 'https://a.b.c/index.html'
        },
        {
          category: 'ExternalViews',
          viewId: 'def',
          label: 'D E F',
          viewUrl: 'https://d.e.f/index.html',
          context: {
            aaaaa: 'hiiiiii'
          }
        }
      ]);
    }
  });
};

var projectDetailNavProviderFn = function (context) {
  return new Promise(function (resolve) {
    var projectId = context.currentProject;
    var children = [{
        category: 'Usermanagement',
        pathSegment: 'users',
        label: 'Users and Groups',
        viewUrl: '/sampleapp.html#/projects/' + projectId + '/users',
        children: [{
            category: 'Groups',
            pathSegment: 'groups',
            label: 'Groups',
            viewUrl: '/sampleapp.html#/projects/' + projectId + '/users/groups',
            children: [{
              pathSegment: ':group',
              viewUrl: '/sampleapp.html#/projects/' +
                projectId +
                '/users/groups/:group',
              context: {
                currentGroup: ':group'
              },
              children: [{
                label: 'Group Settings',
                pathSegment: 'settings',
                viewUrl: '/sampleapp.html#/projects/' +
                  projectId +
                  '/users/groups/:group/settings'
              }]
            }]
          },
          {
            pathSegment: 'usersoverview',
            label: 'Users Overview',
            viewUrl: '/sampleapp.html#/projects/' + projectId + '/users/usersoverview'
          }
        ]
      },
      {
        category: 'Usermanagement',
        pathSegment: 'developers',
        label: 'Developers',
        viewUrl: '/sampleapp.html#/projects/' + projectId + '/developers'
      },
      {
        category: 'Settings',
        pathSegment: 'settings',
        label: 'Project Settings',
        viewUrl: '/sampleapp.html#/projects/' + projectId + '/settings'
      },
      {
        pathSegment: 'miscellaneous',
        constraints: ['unicorns'],
        label: 'Miscellaneous',
        viewUrl: '/sampleapp.html#/projects/' + projectId + '/miscellaneous'
      },
      {
        pathSegment: 'miscellaneous2',
        label: 'Miscellaneous2',
        viewUrl: '/sampleapp.html#/projects/' + projectId + '/miscellaneous2'
      },
      {
        pathSegment: 'misc2-isolated',
        label: 'Miscellaneous2 (Isolated View)',
        isolateView: true,
        viewUrl: '/sampleapp.html#/projects/' + projectId + '/miscellaneous2'
      },
      {
        pathSegment: 'dps',
        label: 'Default Child Node Example',
        defaultChildNode: 'dps2',
        children: [{
            pathSegment: 'dps1',
            label: 'First Child',
            viewUrl: '/sampleapp.html#/projects/' + projectId + '/dps/dps1'
          },
          {
            pathSegment: 'dps2',
            label: 'Second Child',
            viewUrl: '/sampleapp.html#/projects/' + projectId + '/dps/dps2'
          }
        ]
      },
      {
        pathSegment: 'avengers',
        label: 'Keep Selected Example',
        viewUrl: '/sampleapp.html#/projects/' +
          projectId +
          '/dynamic/avengers',
        keepSelectedForChildren: true,
        context: {
          label: 'Avengers',
          links: ['Captain America', 'Iron Man', 'Thor', 'Hulk', 'Black Widow', 'Hawkeye', 'Loki']
        },
        children: ['Captain America', 'Iron Man', 'Thor', 'Hulk', 'Black Widow', 'Hawkeye', 'Loki'].map(name => ({
          pathSegment: name.toLowerCase().split(' ').join('-'),
          label: name,
          context: {
            label: name,
            links: ['Super Power']
          },
          viewUrl: '/sampleapp.html#/projects/' +
            projectId +
            '/dynamic/' + name.toLowerCase().split(' ').join('-'),
          children: [{
            label: 'Super Power',
            pathSegment: 'super-power',
            context: {
              label: 'Super Power',
              links: ['Details']
            },
            viewUrl: '/sampleapp.html#/projects/' +
              projectId +
              '/dynamic/super-power',
            children: [{
              label: 'Details',
              pathSegment: 'details',
              context: {
                label: 'Details',
                links: false
              },
              viewUrl: '/sampleapp.html#/projects/' +
                projectId +
                '/dynamic/details'
            }]
          }]
        }))
      }
    ];
    getProjectPlugins(projectId).then(function (result) {
      result.forEach(function (plugin) {
        children.push({
          category: plugin.category,
          pathSegment: plugin.viewId,
          label: plugin.label,
          viewUrl: plugin.viewUrl,
          context: plugin.context
        });
      });
      resolve(children);
    });
  });
};

var projectsNavProviderFn = function (context) {
  return new Promise(function (resolve) {
    getAllProjects().then(function (result) {
      var children = [];
      result.forEach(function (project) {
        children.push({
          /**
           * navigationContext:
           * Use it for dynamic nodes in order to navigate
           * within a specific context (project in this case)
           * Besides navigate and navigateRelative,
           * LuigiClient provides fromClosestContext().navigate(path)
           * and fromContext(navigationContext).navigate(path) functions
           * which can be used to go upwards multiple context levels
           * eg. /home/:environment/projects/:project/ to go to /home/:environment/settings
           */
          navigationContext: 'project',
          pathSegment: project.id,
          label: project.name,
          viewUrl: '/sampleapp.html#/projects/' + project.id,
          context: {
            currentProject: project.id
          },
          children: projectDetailNavProviderFn
        });
      });
      resolve(children);
    });
  });
};

Luigi.setConfig({
  auth: {
    use: 'mockAuth',
    mockAuth: {
      authorizeUrl: `${window.location.origin}/assets/auth-mock/login-mock.html`,
      logoutUrl: `${window.location.origin}/assets/auth-mock/logout-mock.html`,
      post_logout_redirect_uri: '/logout.html',
      authorizeMethod: 'GET',
      oAuthData: {
        client_id: 'egDuozijY5SVr0NSIowUP1dT6RVqHnlp'
      }
    },
    events: {
      onLogout: function () {
        console.log('onLogout');
      },
      onAuthSuccessful: function (data) {
        console.log('onAuthSuccessful', data);
      },
      onAuthExpired: function () {
        console.log('onAuthExpired');
      },
      // TODO: define luigi-client api for getting errors
      onAuthError: function (err) {
        console.log('authErrorHandler 1', err);
      }
    }
  },
  navigation: {
    nodeAccessibilityResolver: navigationPermissionChecker,
    nodes: function () {
      return [
        {
          pathSegment: 'dashbaord',
          label: 'Dashbaord',
          children: [
            {
              category: 'Overview',
              pathSegment: 'coupons',
              label: 'Coupons',
              viewUrl: 'https://test-coupon-admin.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null,
                globalSettings: _globalSettings
              }
            },
            {
              category: 'Overview',
              pathSegment: 'brands',
              label: 'Brands',
              viewUrl: 'https://test-brand-adminmodule.scapp.io',
              navigationContext: 'brands',
              context: nodedefultObject("saasrepository.brand-admin")       
            },
            {
              category: 'Overview',
              pathSegment: 'commerceSettings',
              label: 'Commerce Settings',
              viewUrl: '/assets/sampleexternal.html#commerce',
              children: [
                {
                  link: '/commerceSettings',
                  label: 'Go back Commerce Settings'
                },
                {
                  pathSegment: 'SiteSettings',
                  label: 'Site Settings',
                  viewUrl: '/assets/sampleexternal.html#email'
                },
                {
                  pathSegment: 'TaxSettings',
                  label: 'Tax Settings',
                  viewUrl: '/assets/sampleexternal.html#email'
                },
                {
                  pathSegment: 'PaymentSettings',
                  label: 'Payment Settings',
                  viewUrl: '/assets/sampleexternal.html#email'
                },
                {
                  pathSegment: 'ShippingSettings',
                  label: 'Shipping Settings',
                  viewUrl: '/assets/sampleexternal.html#email'
                },
                {
                  pathSegment: 'SiteContentSettings',
                  label: 'Site Content Settings',
                  viewUrl: '/assets/sampleexternal.html#email'
                },
                {
                  pathSegment: 'AlgoliaSettings',
                  label: 'Algolia Settings',
                  viewUrl: '/assets/sampleexternal.html#email'
                }
              ]
            },
            {
              category: 'Overview',
              pathSegment: 'content',
              label: 'Content',
              viewUrl: 'https://content-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'countries',
              label: 'Countries',
              viewUrl: 'https://test-country-admin.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'coupon-report',
              label: 'Coupon-Report',
              viewUrl: 'https://coupon-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'customers',
              label: 'Customers',
              viewUrl: 'https://customer-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'delivery-areas',
              label: 'Delivery Areas',
              viewUrl: 'https://deliveryarea-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'delivery-times',
              label: 'Delivery Times',
              viewUrl: 'https://deliverytime-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'fees',
              label: 'Fees',
              viewUrl: 'https://fee-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'labels',
              label: 'Labels',
              viewUrl: 'https://label-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'orders',
              label: 'Orders',
              viewUrl: 'https://order-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'products',
              label: 'Products',
              viewUrl: 'https://pcm-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'picklists',
              label: 'Picklists',
              viewUrl: 'https://picklist-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'reports',
              label: 'Reports',
              viewUrl: 'https://reports-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            },
            {
              category: 'Overview',
              pathSegment: 'suppliers',
              label: 'Suppliers',
              viewUrl: 'https://supplier-admin-dev.scapp.io',
              context: {
                currentLanguage: 'en-us',
                notificationConfig: {autoProcessing: true, autoNotifications: true},
                currentAccountId: null
              }
            }
          ]
        }
        //,
        // {
        //   pathSegment: 'projects',
        //   label: 'Projects1',
        //   viewUrl: '/sampleapp.html#/projects/overview',
        //   children: projectsNavProviderFn
        // },
        // {
        //   pathSegment: 'settings',
        //   label: 'Settings',
        //   viewUrl: '/sampleapp.html#/settings'
        // },
        // ,{
        //   pathSegment: 'styleguide',
        //   label: 'Styleguide',
        //   viewUrl: '/sampleapp.html#/styleguide'
        // }
        // ,{
        //   pathSegment: 'cuponadmin',
        //   label: 'Cupon Admin',
        //   viewUrl: '/sampleapp.html#/cuponadmin'
        // }
      ]
    }
  },
  routing: {
    useHashRouting: true
  },
  builderCompatibilityMode:true,
  settings: {
    header: {
      title: "Back-Office"
    }
  }
});
