'use strict';

var baasApp = angular.module("baasApp",['builder.translations', 'ngSanitize', 'ui.bootstrap']);


baasApp.controller("NavCtrl", function ($scope, $timeout, $location, $http, $translate, $sce, $modal, languageMappings, fallbackLanguage, currentLanguage) {

    $scope.sce = $sce;
    /* ---------------------------------- */
    /* INIT STUFF */
    $scope.navModel = null;
    $scope.nodes = [];
    $scope.dirtyStack = [];
    $scope.selectedTopEntry = null;
    $scope.currentTopEntries = [];
    $scope.currentRootNode = null;
    $scope.mainContextNodes = [];
    $scope.bindings = {};
    $scope.global_settings = {};
    $scope.loading = true;
    $scope.topRightNavLabel = "Choose";
    $scope.topRightShortcuts = {};
    $scope.authenticatedTenant = null;
    $scope.reload=false;
    $scope.viewStackCounter = 0;
    $scope.viewStackCallbacks = {};
    $scope.viewStackData = {};
    $scope.translate = $translate;

    $scope.externalNodesProvider = new BuilderExternalNodes();


    if(!window._currentProjectLocales) {
        window._currentProjectLocales = {};
    }
    var predefinedDefaultLocales = {
        languages: [{'id': 'en', 'label': 'English', 'default': true, 'required': true}],
        currencies: [{'id': 'USD', 'label': 'US Dollar', 'default': true, 'required': true}]
    };

    window._authCallbacks = {};

    window._notificationManager = new __Notificationmanager($timeout, $scope);

    $scope._authManager = function () {
        return new __Authmanager($location);
    };



    $scope.checkConfirmNav = function (ok_cb) {
        if ($scope.dirty) {
            $translate(['NAVIGATION.CONFIRM_NAV_EXIT_TITLE', 'NAVIGATION.CONFIRM_NAV_EXIT_MSG']).then(function (translations) {
                $scope.getNotificationManager().showConfirmation(translations['NAVIGATION.CONFIRM_NAV_EXIT_TITLE'], translations['NAVIGATION.CONFIRM_NAV_EXIT_MSG'], function () {
                    $scope.dirty = false;
                    $scope.deleteLocalDrafts();
                    ok_cb();
                });
            });
        }
        else {
            $scope.deleteLocalDrafts();
            ok_cb();
        }

    };

    $scope.deleteLocalDrafts = function () {
        localStorage.setItem("deleteDrafts", true);
        for (var i = localStorage.length - 1; i >= 0; i--) {
            var key = localStorage.key(i);
            if (isDefinedNotNull(key) && key.indexOf("builder_drafts") === 0) {
                localStorage.removeItem(key);
            }
        }
    };

    /* ---------------------------------- */
    /* GET NAVIGATION TREE MODEL */
    $scope.setNavModel = function(data) {
        $scope.navModel = data;
        if($scope.isDefinedNotNull(data.global_settings)) {
            $scope.global_settings = data.global_settings;
        }

        $scope.nodes = $scope.processNodes(null, $scope.navModel.navModel);
        $scope.homeNode = $scope.navModel.navModel[0];
        $scope.selectRootNode($scope.nodes[0]);

        if(isDefinedNotNull($scope.currentRootNode)) {
            $scope.localizeNode($scope.currentRootNode);
        }

        $scope.topRightShortcuts = {};
        $scope.mainContextNodes = [];
        angular.forEach($scope.nodes, function(node, index) {
            if(node.nodeLocation === "mainContext") {
                $scope.mainContextNodes.push(node);
            }
            if(node.shortcut === "true") {
                $scope.topRightShortcuts[node.ref] = $scope.getName(node);
            }
        });
        $scope.initNavigation();
        if(data.errors !== undefined && $.isArray(data.errors)) {
            angular.forEach(data.errors, function(msg, index) {
                $scope.getNotificationManager().showError(msg);
            });
        }
    };


    $scope.localizeNode = function(node) {

        $scope.localizeNodeLabel(node);
        $scope.localizeNodeTourStep(node);
        if(node.moduleInfo) {
            $scope.localizeNodeTourStep(node.moduleInfo);
        }

        if(isDefinedNotNull(node.nodes)) {
            for(var n in node.nodes) {
                $scope.localizeNode(node.nodes[n]);
            }
        }
    }

    $scope.localizeNodeLabel = function(node) {
        if (node.translations !== undefined) {
            if(node.id == undefined) {
                node.id = node.label;
            }
            var translatedLabel = $scope.translateExtModuleNodeText(node.id, node.translations);
            if(translatedLabel) {
                node.label = translatedLabel;
            }
        } else if(node.translate !== false && node.id !== undefined) {
            $translate('NAVIGATION.NODE.' + node.id.toUpperCase()).then(function (trans) {
                node.label = trans;
            });
        }
    }

    $scope.localizeNodeTourStep = function(node) {
        if (node.tourStep) {
            if (node.translations !== undefined) {
                node.tourStep.heading_label = node.tourStep.heading;
                node.tourStep.text_label = node.tourStep.text;
                var translatedHeading = $scope.translateExtModuleNodeText(node.tourStep.heading, node.translations);
                if (translatedHeading) {
                    node.tourStep.heading_label = translatedHeading;
                }
                var translatedText = $scope.translateExtModuleNodeText(node.tourStep.text, node.translations);
                if (translatedText) {
                    node.tourStep.text_label = translatedText;
                }
            } else if(node.id !== undefined) {
                $translate('NAVIGATION.NODE.TOUR.' + node.id.toUpperCase() + '.HEADING').then(function (trans) {
                    node.tourStep.heading_label = trans;
                });
                $translate('NAVIGATION.NODE.TOUR.' + node.id.toUpperCase() + '.TEXT').then(function (trans) {
                    node.tourStep.text_label = trans;
                });
            }
        }
    }

    $scope.translateExtModuleNodeText = function(key, translations) {
        var preferredLang = currentLanguage.get();
        var languages = [];
        for(var lang in translations) {
            languages.push(lang);
        }

        var translatedLabel;
        //preferred lang
        if(languages.indexOf(preferredLang) != -1) {
            translatedLabel = translations[preferredLang][key];
        }
        if(!translatedLabel) {
            //mappings
            for(var mapping in languageMappings) {
                if(mapping == preferredLang) {
                    if(languages.indexOf(languageMappings[mapping]) != -1) {
                        translatedLabel = translations[languageMappings[mapping]][key];
                    }
                    break;
                }
            }
        }
        if(!translatedLabel) {
            //fallback
            if(languages.indexOf(fallbackLanguage) != -1) {
                translatedLabel = translations[fallbackLanguage][key];
            }
        }

        return translatedLabel;
    };


    $scope.fetchNavigationForCurrentToken = function() {
        $scope.fetchTree();
    }



    $scope.fetchTree = function() {



        $http({method: 'GET', url: './navigation/nodes'}).success(function(data) {
            var splitted = data.split("__remove");
            var extModuleUrl = splitted[0];
            var processedData = splitted[1];
            var loadedModules = $scope.externalNodesProvider.getLoadedExternalModules();
            if (!loadedModules[extModuleUrl] !== undefined) {
                try
                {
                    var extModule = $.ajax({
                        url: extModuleUrl,
                        type: 'GET',
                        async: false
                    }).responseJSON;
                    loadedModules[extModuleUrl] = extModule;
                }
                catch (e) {
                    console.error(e);
                }
            }

            var extNodes = $scope.externalNodesProvider.getExternalModuleNodes($scope.externalNodesProvider.getLoadedExternalModules());
            var extNodesString = "";

            for(var i = 0; i < extNodes.length; i++) {
                var nodeStr = JSON.stringify(extNodes[i]);
                extNodesString = extNodesString.concat(nodeStr).concat(",");
            }

            var resultingJsonString = processedData.replace(/__testproject/g,window._currentProjectId);

            resultingJsonString = resultingJsonString.replace(/__extModules__/g, extNodesString);

            $scope.setNavModel(JSON.parse(resultingJsonString));
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            if(status == '401') {
                $scope._authManager().handleMissingToken();
            } else {
                $scope.loading = false;
                $scope.getNotificationManager().showError("Navigation could not be loaded : "+data.url+" - "+data.errorMessage);
            }
        });
    };

    $scope.loadNodeChildren = function (node)
    {
        $scope.getNotificationManager().pushProcessing();
        var loadChildrenUrl = node.lazyLoadNodesUrl;
        $http.defaults.headers.common.Authorization = 'Bearer '+$scope._authManager().getAccessToken();
        $http({method: 'GET', url: loadChildrenUrl})
            .success(function(data){
                node.nodes = data.childNodes;
                node.lazyLoadNodesUrl = null;
                if(data.errors !== undefined && $.isArray(data.errors)) {
                    angular.forEach(data.errors, function(msg, index) {
                        $scope.getNotificationManager().showError(msg);
                    });
                }
                $scope.processNodes(node, node.nodes);
                $scope.initNavigation();
                $scope.getNotificationManager().popProcessing();

            }).error(function(data, status, headers, config) {
                if(status == '401') {
                    $scope._authManager().handleMissingToken();
                }
                else {
                    $scope.getNotificationManager().showError("Navigation could not be loaded : "+data.url+" - "+data.errorMessage);
                }
            });
    }



    /* ---------------------------------- */
    /* INIT STUFF */
    $scope.processNodes = function(parentNode, nodes) {
        if(!$scope.isDefinedNotNull(parentNode)) {
            $scope.allNodePaths = [];
        }
        if($scope.isDefinedNotNull(nodes)) {
            var parentPath = parentNode?parentNode.path:"";
            var parentDepth = parentNode?parentNode.depth:-1;
            for(var i=0;i<nodes.length;i++) {
                if(nodes[i].shortcut==="true" || nodes[i].divider==="true") {
                    continue;
                }
                nodes[i].path=parentPath + "/" + nodes[i].id;
                nodes[i].depth = parentDepth + 1;
                nodes[i].parentNode = parentNode;
                if(parentNode && parentNode.virtualChildren==="true") {
                    nodes[i].depth--;
                }
                if(nodes[i].virtualChildren==="true") {
                    nodes[i].class="navZeroWidth";
                }
                $scope.allNodePaths.push(nodes[i].path);
                $scope.processNodes(nodes[i], nodes[i].nodes);
            }
        }
        return nodes;
    };
    /* ---------------------------------- */


    /* ---------------------------------- */
    /* OVERALL NAVIGATION FUNCTIONALITY */

    $scope.initNavigation = function() {
        var searchObject = $location.search();
        var pathString = searchObject['selectedPath'];
        var referrer = searchObject['referrer'];
        var node = $scope.nodes[0];

        if($scope.isDefinedNotNull(pathString)) {
            var path = $scope.splitPath(pathString);
            if(path.length > 0) {
                node = $scope.getLastNode(path, 0, $scope.nodes);
            }
        }
        if(node) {

            if ($scope.moduleFullScreenOn && !$scope.isExternalNode(node)) {
                $scope.turnModuleFullScreenOff();
            }

            if($scope.isDefinedNotNull(node.dynamic)) {
                node.dynpath=pathString;
            }
            var prevContext = {};
            $scope.bindings = {};
            $scope.setBindings(node);
            var newContext = {};
            newContext.currentProjectId = window._currentProjectId;
            newContext.currentTeamId = window._currentTeamId;
            if(!$scope.reload) {
                if ($scope.isLazyNode(node)) {
                    $scope.loadNodeChildren(node);
                    return;
                }

                var contentNode = $scope.adjustNavigation(node);

                var contentUrl = contentNode.url;
                if ($scope.isDefinedNotNull(contentNode.extUrl)) {
                    contentUrl = contentNode.extUrl;
                }
                if (!$scope.isDefinedNotNull(contentUrl) && !$scope.isDefinedNotNull(contentNode.nodes)) {
                    var title = encodeURIComponent($scope.getName(contentNode));
                    contentUrl = "iframe/undcon.html?" + title + "#" + title;
                }

                if ($scope.isDefinedNotNull(contentUrl)) {
                    var contentUrlMod = contentUrl;
                    var hashIndex = contentUrl.indexOf("#");
                    if (hashIndex >= 0) {
                        contentUrlMod = contentUrl.substring(0, hashIndex);
                    }
                    if ($scope.isDefinedNotNull(referrer)) {
                        contentUrlMod = contentUrlMod + (contentUrlMod.indexOf('?') < 0 ? '?' : '&') + 'referrer=' + encodeURIComponent(referrer);
                    }
                    if (hashIndex >= 0) {
                        contentUrlMod = contentUrlMod + contentUrl.substring(hashIndex);
                    }
                    $scope.setContentUrl(contentUrlMod, contentNode);
                    $timeout(function () {
                        if ($scope.isDefinedNotNull(node.parentNode) && node.parentNode.nodeLocation !== "top") {
                            var wrapper = $('.wrapper');
                            if (wrapper.hasClass('show-canvas')) {
                                wrapper.toggleClass('show-canvas');
                            }
                        }
                    });
                }
            }

            // reset notifications
            $scope.notifications = [];
        }
    };

    $scope.isExternalNode = function(node) {
        return !!node.moduleInfo;
    };

    $scope.isLazyNode = function(node) {
        return false;
    };

    $scope.authorizeForTenant = function(tenantId, state) {
        $scope.reload=false;
    };

    $scope.handleProjectUserLocales = function (projectId) {
        /* REMOVED */
    };

    $scope.handleProjectLocales = function() {
        /* removed */
    };

    $scope.handleUserLocales = function() {
        /* removed */
    };

    $scope.activateNode = function(node) {
        $scope.checkConfirmNav(function() {
           $scope.activateNodeInternal(node);
        });
    };

    $scope.activateNodeInternal = function(node) {
        if($scope.isDefinedNotNull(node)) {
            if($scope.isDefinedNotNull(node.unnavigable) && node.unnavigable==true) {
                return;
            }
            var path = node.path;
            if(node.shortcut==="true") {
                path = node.ref;
            }
            var selectedPath = $location.search().selectedPath;
            // "r" param ensures that the URL is different even if the same node is clicked
            // so that it always reloads the view
            var r = $location.search().r;
            $location.search("selectedPath", path);
            $location.search("referrer", null);
            $location.search("bck", null);
            $location.search("pv", null);
            $location.search("r", null);
            var newSelectedPath = $location.search().selectedPath;
            if (selectedPath == newSelectedPath) {
                $location.search("r", r ? null : "1");
            }
        }
    };

    $scope.adjustNavigation = function(node) {
        if($scope.isDefinedNotNull(node)) {
            var parent = node.parentNode;

            $scope.setCurrentRootNode($scope.getRootNode(node));

            if(!$scope.isDefinedNotNull(parent)) {
                $scope.selectRootNode(node);
            }
            else if(node.nodeLocation === "top") {
                $scope.setLeftNavNode(node, -1);
                $scope.selectedTopEntry = node.nodes[$scope.isDefinedNotNull(node.initialSelect)?node.initialSelect:0];
                return $scope.selectedTopEntry;
            }
            else if(parent.nodeLocation === "top") {
                $scope.setLeftNavNode(parent, -1);
                $scope.selectedTopEntry = node;
            }
            else if(parent.virtualChildren === "true") {
                $scope.setLeftNavNode(parent, -1);
            }
            else if($scope.isDefinedNotNull(node.url) || $scope.isDefinedNotNull(node.extUrl) || !$scope.isDefinedNotNull(node.nodes)) {
                $scope.setLeftNavNode(node, -1);
            }
            else if($scope.isDefinedNotNull(node.initialSelect)
                && $scope.isDefinedNotNull(node.nodes)
                && node.initialSelect < node.nodes.length) {
                var fwdNode = node.nodes[node.initialSelect];
                $scope.setLeftNavNode(fwdNode, -1);
                return fwdNode;
            }
            else {
                $scope.setLeftNavNode(node, 0);
            }

            if($scope.isDefinedNotNull(node.initialSelect)) {
                return $scope.adjustNavigation(node.nodes[node.initialSelect]);
            }
        }
        return node;
    };

    $scope.setBindings = function(node) {
        if($scope.isDefinedNotNull(node)) {
            if($scope.isDefinedNotNull(node.bind)) {
                if(typeof node.bind === "string") {
                    window[node.bind] = node.id;
                    $scope.bindings[node.bind] = node;
                } else {
                    for (var key in node.bind) {
                        if (node.bind.hasOwnProperty(key)) {
                            var val = node.bind[key];
                            window[key] = val;
                            $scope.bindings[key] = node;
                        }
                    }
                }
            }
            $scope.setBindings(node.parentNode);
        }
    };


    $scope.setLeftNavNode = function(node,offset) {
        $timeout(function() {
            var _offset = offset?offset:0;
            var nav = jQuery(".vertical-nav.secondary-navigation");

            var navDepth = node.depth + _offset;
            var navLeft = navDepth * nav.offsetParent().width();

            nav.css({ 'left': '-'+navLeft+'px' });

            var previousActiveNode = nav.find('.active .current').find('a:first').attr('baas-node-id');

            nav.find(".active").removeClass("active").removeClass("open");
            nav.find("li").addClass("current");
            nav.find("._current").removeClass("_current");

            var currentPath = node.path;

            var visibleNode = node;

            while($scope.isDefinedNotNull(visibleNode)) {
                if(visibleNode.depth === navDepth) {
                    break;
                }
                visibleNode = visibleNode.parentNode;
            }

            while(currentPath.length > 0) {
                var selector = "[baas-node-id='"+currentPath+"']";
                var parentLI = jQuery(selector).parent();

                if($scope.isDefinedNotNull(visibleNode) && visibleNode.path === currentPath) {
                    if(parentLI.length > 0) {
                        parentLI.addClass("_current");
                    } else {
                        jQuery(".vertical-nav.secondary-navigation .nav").addClass("_current");
                    }
                }

                parentLI.addClass("active");
                parentLI.addClass("open");
                var index = currentPath.lastIndexOf("/");
                if(index > 0) {
                    currentPath = currentPath.substr(0,index);
                }
                else {
                    currentPath = "";
                }
            }

            //mark previous team/project node as 'active' when going back to list of teams/projects
            if (_offset == 0) {
                nav.find("[baas-node-id*='" + previousActiveNode + "']").first().parent().addClass('active');
            }

            $timeout(function() {
                nav.find(".current").removeClass("current");
                nav.find("._current").addClass("current");
                nav.height(jQuery('.current > ul').height());
            }, 250);

        });
    };


    $scope.setCurrentRootNode = function(node) {
        $scope.setBindings(node);
        $scope.currentRootNode = node;
    };

    $scope.selectRootNode = function(node) {
        $scope.setCurrentRootNode(node);
        var nav = jQuery(".vertical-nav.secondary-navigation");
        nav.css({ 'left': '0' });
        nav.find(".active").removeClass("active").removeClass("open");
        nav.find(".nav").addClass("current");
    };

    $scope.setContentUrl = function(url, node) {
        var name = "__home";
        var _node = node;
        if($scope.isDefinedNotNull(node)) {
            name = node.path;
        }
        else {
            _node = $scope.navModel.homeNode;
        }

        if(!$scope.isDefinedNotNull(window._cng_widgets[name])) {
            var widget = {};
            widget.settings = $.extend({}, $scope.global_settings, $scope.isDefinedNotNull(_node.settings) ? _node.settings : {});
            window._cng_widgets[name] = widget;
        }

        if($scope.isDefinedNotNull(node.dynamic)) {
            window._cng_widgets[name].settings[node.dynamic] = node.dynamicValue;
            url = url.replace("{"+node.dynamic+"}", node.dynamicValue);
        }

        var remoteNavigation = false;
        if($scope.isDefinedNotNull(node.remoteNavigation)) {
            remoteNavigation = node.remoteNavigation;
        }

        var frameCnt = $(".iframeWrapper");
        $timeout(function () {

            if (($location.search())["bck"] === '1') {
                if ($scope.goBackInternal(node)) {
                    return;
                }
            }

            var currentWidget = window._cng_widgets[name];
            var clientId = localStorage.getItem("dev-currentModuleId") ;

            if (($location.search())["pv"] === '1') {
                $scope.viewStackCounter += 1;
                frameCnt.children().hide();
                frameCnt = removeFramesAfter(frameCnt, $scope.viewStackCounter - 2);
            } else {
                $scope.viewStackCounter = 1;
                frameCnt = removeFramesAfter(frameCnt, 0);
            }
            var newFrame = $('#cntFrame' + $scope.viewStackCounter);

            $scope.getNotificationManager().clearProcessing();
            $scope.getNotificationManager().clearConfirmation();

            var processed = false;
            if ($scope.isDefinedNotNull(newFrame) && newFrame.length > 0) {
                var previousClientId = newFrame.attr('client-id');
                var previousRemoteNavigation = newFrame.attr('remote-navigation');
                if (remoteNavigation && previousRemoteNavigation && previousClientId == clientId && $scope.isDefinedNotNull(clientId) && clientId != '') {
                    newFrame.show();
                    document.getElementById("cntFrame"+$scope.viewStackCounter).contentWindow.postMessage(["navigation", JSON.stringify({"url":url})], "*");
                    processed = true;
                } else {
                    frameCnt = removeFramesAfter(frameCnt, $scope.viewStackCounter - 2);
                }
            }
            if (!processed)
            {
                frameCnt.append('<iframe scrolling="no" style="display: none;" src="' + url + '" id="cntFrame' + $scope.viewStackCounter + '" frameborder="0" name="' + name +
                    '"allowTransparency="true" nodepath="' + (node.dynpath ? node.dynpath : name) + '"'
                    + (($scope.isDefinedNotNull(clientId) && clientId != '') ? ' client-id="' + clientId + '"' : '')
                    + (remoteNavigation ? ' remote-navigation="true"' : '') + '></iframe>');

                var referrerHash = ($location.search())['referrer'];
                var referrer;
                if(isDefinedNotNull(referrerHash)){
                    referrer = referrerHash.replace(/[^-A-Za-z0-9 +@/%=~_|!:,.;\(\)]/g, '');
                }

                $('#cntFrame'+$scope.viewStackCounter).load(function() {
                    $('#cntFrame'+$scope.viewStackCounter).show();
                    // prepare data
                    var transferObject = new Object();
                    transferObject.globalSettings = window._globalSettings;
                    transferObject.notificationConfig = { autoProcessing: true, autoNotifications: true };
                    transferObject.currentWidget = window._cng_widgets[name];
                    transferObject.currentAccountId = window._currentAccountId;
                    transferObject.currentTeamId = window._currentTeamId;
                    transferObject.currentProjectId = window._currentProjectId;
                    if($scope.isDefinedNotNull(transferObject.currentProjectId) && $scope.isDefinedNotNull($scope.bindings['_currentProjectId'])) {
                        transferObject.currentProjectPath = $scope.bindings['_currentProjectId'].path;
                    }
                    transferObject.currentProjectLocales = window._currentProjectLocales;
                    transferObject.currentLocation = name;
                    transferObject.allNodePaths = $scope.allNodePaths;

                    transferObject._referrer = referrer;

                    transferObject.missingDependencies = [];

                    transferObject.currentWidget.settings._client_id = window._currentModuleClientId;
                    transferObject.currentWidget.settings._module_id = window._moduleId;

                    transferObject.helpMode = $scope.isHelpModeActive();

                    transferObject._hasBack = $scope.viewStackCounter > 1;

                    transferObject.currentLanguage = window._currentLanguage ? window._currentLanguage : "en";

                    transferObject.accessToken = $scope._authManager().getAccessToken();
                    transferObject.scope = $scope._authManager().getScope();

                    if(node.moduleInfo && node.moduleInfo._packages) {
                        transferObject._packages = node.moduleInfo._packages;
                    }

                    document.getElementById("cntFrame"+$scope.viewStackCounter).contentWindow.postMessage(["init", JSON.stringify(transferObject)], "*");
                });
            }

        }, 250);
    };

    var removeFramesAfter = function(frameCnt, index) {
        var modified = false;
        for(var i = frameCnt.children().length - 1; i > index; i--) {
            frameCnt.children()[i].remove();
            modified = true;
       }
       return(modified == true ? $(".iframeWrapper") : frameCnt);
    }

    $scope.popView = function(data) {
        if($scope.viewStackCounter > 1) {
            $timeout(function () {
                $scope.checkConfirmNav(function() {
                    $scope.viewStackData[$scope.viewStackCounter] = data;
                    var newFrame = $('#cntFrame'+ ($scope.viewStackCounter - 1));
                    var newNodePath = newFrame.attr("nodepath");
                    $location.search("bck", "1");
                    $location.search("selectedPath", newNodePath);
                });
            });
        }
    };

    $scope.goBackInternal = function (node) {
        if($scope.viewStackCounter > 0) {
            var newFrameId = 'cntFrame' + ($scope.viewStackCounter - 1);
            var newFrame = $('#' + newFrameId);
            var newNodePath = newFrame.attr("nodepath");
            if ($scope.isDefinedNotNull(newNodePath) && (node.path === newNodePath || node.dynpath === newNodePath)) {
                $('#cntFrame' + $scope.viewStackCounter).remove();
                var callback = $scope.viewStackCallbacks[$scope.viewStackCounter];
                $scope.viewStackCallbacks[$scope.viewStackCounter] = null;
                var data = $scope.viewStackData[$scope.viewStackCounter];
                $scope.viewStackData[$scope.viewStackCounter] = null;
                $scope.viewStackCounter -= 1;
                newFrame.show();
                if ($scope.isDefinedNotNull(callback)) {
                    callback(data);
                } else {
                    try
                    {
                        document.getElementById(newFrameId).contentWindow.postMessage(["goBackCallback", JSON.stringify(data)], "*");
                    }
                    catch (e) {
                        console.error(e);
                    }
                }
                return true;
            }
        }
        return false;
    };


    $scope.toggleHelpMode = function () {
        if ($scope.isHelpModeActive()) {
            window._helpMode = false;
        }
        else {
            window._helpMode = true;
        }
        localStorage.setItem('helpMode', window._helpMode);

        $(".iframeWrapper iframe").each(function (index, iframeElement) {
            iframeElement.contentWindow.postMessage(["helpModeChanged", window._helpMode], "*");
        });
    };

    $scope.isHelpModeActive = function() {
        return localStorage.getItem('helpMode') === 'true';
    };



    /* --------------------------*/
    /* UTILITY METHODS           */
    $scope.getLastNode = function(path, currentPathIndex, currentNodes) {
        if(currentPathIndex < path.length) {
            for (var i = 0; i < currentNodes.length; i++) {
                var cNode = currentNodes[i];
                var dyn = $scope.isDefinedNotNull(cNode.dynamic);
                if (path[currentPathIndex] === cNode.id || dyn) {
                    if (dyn) {
                        cNode['dynamicValue'] = path[currentPathIndex];
                    }

                    if ($scope.isDefinedNotNull(cNode.nodes)) {
                        var gcNode = $scope.getLastNode(path, currentPathIndex + 1, cNode.nodes);
                        if ($scope.isDefinedNotNull(gcNode)) {
                            return gcNode;
                        }
                    }
                    return cNode;
                }
            }
        }
        return null;
    };

    $scope.splitPath = function(pathString) {
        if(pathString[0]==='/') { pathString = pathString.substr(1); }
        return pathString.split('/');
    };


    $scope.getRootNode = function(node) {
        if(!$scope.isDefinedNotNull(node)) {
            return null;
        }
        else if($scope.isDefinedNotNull(node.parentNode)) {
            return $scope.getRootNode(node.parentNode);
        }
        else {
            return node;
        }
    };

    $scope.isLeftNavLeave = function(node) {
        return !$scope.isDefinedNotNull(node.nodes) || node.nodeLocation==='top';
    };

    $scope.isDefinedNotNull = function(val) {
        return typeof val !== 'undefined' && val !== null;
    };

    $scope.getName = function(node, dontShowContextNode) {
        if($scope.isDefinedNotNull(node) && (dontShowContextNode !== false || node.nodeLocation!=="mainContext")) {
            if($scope.isDefinedNotNull(node.label)) {
                return node.label;
            }
            else {
                return node.id;
            }
        }
        return "Choose";
    };

    $scope.getBackName = function(node) {
        return "Back to " + $scope.getName(node.parentNode);
    };
    /* --------------------------*/

    $scope.openFeedback = function() {
        $("a.feedback-affix").trigger("click");
    };

    $scope.$on('$locationChangeSuccess', function() {
        $scope.initNavigation();
        if(window.ga !== undefined) {
            ga('send', 'pageview', {
                'page': $location.search()['selectedPath'] ? $location.search()['selectedPath'] : ''
            });
        }
    });

    jQuery(document).ready(function($) {


        if(parent && parent.location.href != window.location.href)
        {
            parent.location.href = window.location.href;
        }


        window._cng_widgets = {};
        $scope.currentAccountId = window._currentAccountId;
    });

    jQuery('.nav-super-toggle').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.nav-super-dropdown').removeClass('open');
        }else{
            $(this).addClass('active');
            $('.nav-super-dropdown').addClass('open');
        }

    });

    $scope.getNotificationManager = function() {
        return window._notificationManager;
    };

    $scope._linkManager = function () {
        return new __Linkmanager($scope,$location);
    };

    $scope._getToken = function(force) {
        $.ajax('./accesstoken',
            {
                "type": 'GET',
                "async" : false,
                "success" : function(response) {
                        if("no_token" !== response) {
                            var sinfo = JSON.parse(response.toString());
                            var am = new __Authmanager();
                            am.setCurrentUser(sinfo.username);
                            window._expDate = sinfo.validUntil;

                            if(localStorage.getItem("authpropsTimestamp") || force){
                                if(localStorage.getItem("authpropsTimestamp")!==sinfo.timestamp || force) {
                                    localStorage.setItem("authpropsTimestamp", sinfo.timestamp);
                                    localStorage.setItem("hasTimestampChanged", true);
                                    am.setCurrentAccountId(sinfo.username);
                                    am.setCurrentModuleClientId(sinfo.clientId);
                                    am.setCurrentProjectId(sinfo.tenant);
                                    am.setScope(sinfo.scopes);
                                    am.setAccessToken(sinfo.token);
                                }else{
                                    localStorage.setItem("hasTimestampChanged", false);
                                }
                            }else{
                                localStorage.setItem("authpropsTimestamp",sinfo.timestamp);
                                localStorage.setItem("hasTimestampChanged", true);
                            }

                        }
                },
                "error" : function(jqXHR, textStatus, theError) {
                    console.log(theError);
                }
            }
        );
    };

    window._linkManager = $scope._linkManager;
    window._authManager = $scope._authManager;
    window._getToken = $scope._getToken;

    $scope._getToken();
    $scope.fetchNavigationForCurrentToken();

    $scope.validMgrCalls = {
        authMgr : ["authError"],
        notificationMgr : ["pushProcessing","popProcessing","clearProcessing",
            "addNotification","showSuccess","showError","showWarning","showInfo","showModalDialog","setPageDirty"],
        linkMgr : ["open","goBack"]
    };


    /* --------------------------*/
    /*    MODULE'S FULLSCREEN    */

    $scope.turnModuleFullScreenOn = function() {
        var contentFrame = $('.frameCnt');
        contentFrame.addClass('builder-module-maximized');
        $scope.moduleFullScreenOn = true;
        postFullScreenMsgToModule('on');
    };

    $scope.turnModuleFullScreenOff = function() {
        var contentFrame = $('.frameCnt');
        contentFrame.removeClass('builder-module-maximized');
        $scope.moduleFullScreenOn = false;
        postFullScreenMsgToModule('off');
    };

    var postFullScreenMsgToModule = function(msg) {
        $(".iframeWrapper iframe").each(function (index, iframeElement) {
            iframeElement.contentWindow.postMessage(["fullScreenModeState", msg], "*");
        });
    };

    window.addEventListener("message", function(e) {
        if($.isArray(e.data)) {
            var id = e.data[0];
            var payload = JSON.parse(e.data[1]);

            var pmsg = payload.msg;
            var pdata = payload.data;

            if(id === "authManager") {
                if($scope.validMgrCalls.authMgr.indexOf(pmsg) >= 0 ) {
                    if(pmsg === "authError") {
                        window._authManager().handleAuthError(pdata.navigationState, pdata.response);
                    }
                } else {
                    console.error("Blocked invalid call to authMgr from external module: " + pmsg);
                }
            }
            if(id === "notificationManager") {
                if($scope.validMgrCalls.notificationMgr.indexOf(pmsg) >= 0 ) {
                    window._notificationManager.execute(pmsg, pdata, e);
                } else {
                    console.error("Blocked invalid call to notificationManager from external module: " + pmsg);
                }
            } else if(id === "linkManager") {
                if($scope.validMgrCalls.linkMgr.indexOf(pmsg) >= 0 ) {
                    window._linkManager().execute(pmsg, pdata);
                } else {
                    console.error("Blocked invalid call to linkManager from external module: " + pmsg);
                }
            } else if (id === "maximizeMainWidget") {
                if ($scope.extensibleMainWidgetUUID === payload.uid) {
                    var mainWidget = $("[builder-dynamic-height][builder-module-uid='" + payload.uid + "']");
                    if (payload.mode === true)
                        mainWidget.addClass("dashboard-mainframe-maximized");
                    else
                        mainWidget.removeClass("dashboard-mainframe-maximized");
                }
                else {
                    console.error("Event maximizeMainWidget can only be sent from main widget!");
                }
            } else if (id === "builderModuleFullScreen") {
                if (pmsg === "on") {
                    $modal.open({
                        animation: true,
                        templateUrl: '/public/templates/fullscreen-module-modal.html',
                        scope: $scope
                    });
                }
                else if (pmsg === "off") {
                    $scope.turnModuleFullScreenOff();
                }
            } else if (id === "resizeModule") {
                var frameCnt = $("[builder-dynamic-height][builder-module-uid='" + payload.uid + "']");
                if (frameCnt.length === 0) {
                    console.error("Could not find module to resize: [builder-dynamic-height][builder-module-uid='" + payload.uid + "']");
                }
                else {
                    if (frameCnt.find("iframe").attr("src").indexOf(e.origin+"/") === 0) { // security check
                        frameCnt.css("height", payload.height + "px");
                    }
                    else {
                        console.error("Could not find module to resize: " + pmsg);
                    }
                }
            } else if (id === "sendExtEvent") {
                var others = $("[builder-module-uid]").not("[builder-module-uid='" + payload.uid + "']");
                others.find("iframe").each(function (index, iframeElement) {
                    iframeElement.contentWindow.postMessage(["sendExtEvent", JSON.stringify(payload)], "*");
                });
            } else if (id === "pluginEvent") {
                if(pmsg === "loaded") {
                    var cb = _authCallbacks[pdata.id];
                    if(cb) {
                        cb();
                        _authCallbacks[pdata.pluginId] = undefined;
                    }
                }
            }
        }
    });
});

baasApp.directive("bindAndCompileHtml", function($parse, $sce, $compile) {
    return {
        restrict: "A",
        link: function (scope, element, attributes) {

            var expression = $sce.parseAsHtml(attributes.bindAndCompileHtml);

            var getResult = function () {
                return expression(scope);
            };

            scope.$watch(getResult, function (newValue) {
                var linker = $compile(newValue);
                element.append(linker(scope));
            });
        }
    }
});



