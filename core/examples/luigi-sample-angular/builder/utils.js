"use strict";

var isDefinedNotNull = function(val) {
    return typeof val !== 'undefined' && val !== null;
};

var arraysContainSameElements = function(array1, array2)
{
    if(isDefinedNotNull(array1) && isDefinedNotNull(array2)) {
        if (array1.length === array2.length) {
            array1.sort();
            array2.sort();
            return _.isEqual(array1, array2);
        }
    }
    return false;
};

var crop =function(text, length)
{
    if (length == undefined){
        length = 255;
    }
    if (text!==null && text!== undefined && text.length >= length)
    {
        return text.substring(0,length - 1) + "...";
    }
    return text;
};


var isMobileView = function(window) {
    return window.matchMedia('(max-width: 768px)').matches;
};

var removeEmptyRedirectUris = function(application) {
    _.forEach(application.redirectUris, function (redirectUri) {
        if ("" === redirectUri) {
            var index = application.redirectUris.indexOf(redirectUri);
            application.redirectUris.splice(index, 1);
        }
    });
    return application;
};